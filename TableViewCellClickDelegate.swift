//
//  TableViewCellClickDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 10/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol TableViewCellClickDelegate: NSObjectProtocol {
    
    func onTableViewCellClick(item: NSObject, cell : UITableViewCell)
    
}
