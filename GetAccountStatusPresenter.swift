//
//  GetAccountStatusPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class GetAccountStatusPresenter: BasePresenter {
    var mAccountDelegate : GetAccountStatusDelegate?
    var request : Alamofire.Request?
    
    init(delegate : GetAccountStatusDelegate) {
        super.init()
        self.mAccountDelegate = delegate
    }
    
    
    func stateAccount(No_Account:String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mAccountDelegate?.onSendingAccount()
            
            let stateAccountRequest : GetAccountStatusInfoRequest = GetAccountStatusInfoRequest()
            
            stateAccountRequest.login = Login()
            stateAccountRequest.login?.user = "25631"
            stateAccountRequest.login?.password = "Middle100$"
            stateAccountRequest.login?.ip = "Ip11"
            stateAccountRequest.accountNumber = No_Account
            
            let params  = Mapper().toJSON(stateAccountRequest)
            
            request = Alamofire.request(Urls.API_GET_INFO_ACCOUNT, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<GetAccountStatusInfoResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : GetAccountStatusInfoResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                        self.mAccountDelegate?.onSuccessAccount(clientInfo: objectReponse.clientInfo!,infoAccount: objectReponse.infoAccount!, infoStatusAccount: objectReponse.accountStatusInfo!,billingHistory: objectReponse.billingHistory,bankReferences: objectReponse.bankReferences!)
                    }else{
                        if(objectReponse.Result?.Description == nil){
                           self.mAccountDelegate?.onErrorAccount(errorMessage: "Error al cargar los datos")
                        }else{
                          self.mAccountDelegate?.onErrorAccount(errorMessage: (objectReponse.Result?.Description)!)
                        }
                        
                    }
                case .failure(_): break
                self.mAccountDelegate?.onErrorAccount(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mAccountDelegate?.onErrorConnection()
        }
    }
}

