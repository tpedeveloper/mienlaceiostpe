//
//  RegisterAccountPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class RegisterAccountPresenter: BasePresenter {
    var mRegisterDelegate : RegisterAccountDelegate?
    var request : Alamofire.Request?
    
    init(delegate : RegisterAccountDelegate) {
        super.init()
        self.mRegisterDelegate = delegate
    }
    
    
    func registerAccount(account:String, phone: String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mRegisterDelegate?.onSendingRegister()
            
            let registerRequest : RegisterAccountRequest = RegisterAccountRequest()
            
            registerRequest.login = Login()
            registerRequest.login?.user = "25631"
            registerRequest.login?.password = "Middle100$"
            registerRequest.login?.ip = "1.1.1.1"
            registerRequest.account = account
            registerRequest.phone = phone
            
            let params  = Mapper().toJSON(registerRequest)
            
            request = Alamofire.request(Urls.API_REGISTER_ACCOUNT, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<LoginResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : LoginResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                        self.mRegisterDelegate?.onSuccessRegister(succesMessage: (objectReponse.Result?.Description)!)
                    }else{
                        self.mRegisterDelegate?.onErrorRegister(errorMessage: (objectReponse.Result?.Description)!)
                    }
                case .failure(_): break
                self.mRegisterDelegate?.onErrorRegister(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mRegisterDelegate?.onErrorConnection();
        }
    }
}

