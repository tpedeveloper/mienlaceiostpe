//
//  FormalityEntity.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 06/06/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//
import UIKit
import ObjectMapper
import RealmSwift

public class FormalityEntity : Object, Mappable {
    
    dynamic var accountNumber : String?
   
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
    }
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}

