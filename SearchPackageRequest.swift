//
//  SearchPackageRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchPackageRequest : NSObject, Mappable{
    var accountNumber : String?
    var arrLogin : Login?
    
    override init() {
        super.init()
    }
    
    init(accountNumber: String, arrLogin: Login) {
        self.accountNumber = accountNumber
        self.arrLogin=arrLogin
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        accountNumber <- map["ACCOUNT_NO"]
        arrLogin <- map["arrLogin"]
    }

}
