//
//  BillingHistory.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class BillingHistory : NSObject, Mappable{
    
    dynamic var fecha: String?
    dynamic var saldo: String?
    dynamic var url: String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        fecha		<- map["Fecha"]
        saldo		<- map["Saldo"]
        url		<- map["URL"]
    }
}
