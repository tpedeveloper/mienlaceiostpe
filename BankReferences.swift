//
//  BankReferences.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class BankReferences : NSObject, Mappable{
    
    dynamic var Spei: String?
    dynamic var BancoAzteca: String?
    dynamic var Bancomer: String?
    dynamic var Scotianbank: String?
    dynamic var Santander: String?
    dynamic var Banorte: String?
    dynamic var Banamex: String?
    dynamic var Hsbc: String?
    
    

    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Spei		<- map["Spei"]
        BancoAzteca		<- map["BancoAzteca"]
        Bancomer		<- map["Bancomer"]
        Scotianbank		<- map["Scotiabank"]
        Santander		<- map["Santander"]
        Banorte		<- map["Banorte"]
        Banamex		<- map["Banamex"]
        Hsbc		<- map["Hsbc"]
    }
    
    
}
