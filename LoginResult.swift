//
//  LoginResult.swift
//  MiEnlaceTP
//
//  Created by fer on 23/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginResult  : NSObject, Mappable {
    dynamic  var idResult : String?
    dynamic  var result : String?
    dynamic  var Description : String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        result		<- map["Result"]
        idResult		<- map["IdResult"]
        Description		<- map["Description"]
    }

    
}

