//
//  SearchPackagePresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class SearchPackagePresenter: BasePresenter {
    var mSearchPackageDelegate : SearchPackageDelegate?
    var request : Alamofire.Request?
    
    var dnArray = [String]()
    var servInterArray = [String]()
    var servPhoneArray = [String]()
    
    
    
    init(delegate : SearchPackageDelegate) {
        super.init()
        self.mSearchPackageDelegate = delegate
    }
    
    
    func searchPackage(account:String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mSearchPackageDelegate?.onSendingSearchPackage()
            
            let searchPackageRequest : SearchPackageRequest = SearchPackageRequest()
            
            searchPackageRequest.arrLogin = Login()
            searchPackageRequest.arrLogin?.user = "25631"
            searchPackageRequest.arrLogin?.password = "Middle100$"
            searchPackageRequest.arrLogin?.ip = "1"
            searchPackageRequest.accountNumber = account
            
            
            let params  = Mapper().toJSON(searchPackageRequest)
            
            request = Alamofire.request(Urls.API_SEARCH_PACKAGE, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<SearchPackageResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : SearchPackageResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                        
                        let total:Int = objectReponse.services.count - 1
                        
                        for i in (0..<total)
                        {
                            if(objectReponse.services[i].Service == "telephony"){
                                self.dnArray.append(objectReponse.services[i].DN!)
                            }
                            
                            let total_prod: Int = objectReponse.services[i].Productos.count
                            for y in (0..<total_prod){
                                if (objectReponse.services[i].Service == "telephony") {
                                    self.servPhoneArray.append(objectReponse.services[i].Productos[y].Description!)
                                } else if (objectReponse.services[i].Service == "ip") {
                                    self.servInterArray.append(objectReponse.services[i].Productos[y].Description!)
                                }
                            }
                        }
                        
                        self.mSearchPackageDelegate?.onSuccessSearchPackage(additional_fees: objectReponse.additionalFees!, arrayServPhone: self.servPhoneArray, arrayServInter: self.servInterArray, arrayDn:self.dnArray , plan: objectReponse.Plan!, renta:objectReponse.Renta!)
                    }else{
                        self.mSearchPackageDelegate?.onErrorSearchPackage(errorMessage: "Ocurrio un error al consultar los planes")
                    }
                case .failure(_): break
                self.mSearchPackageDelegate?.onErrorSearchPackage(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mSearchPackageDelegate?.onErrorConnection()
        }
    }
}
