//
//  ChangePasswordPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class ChangePasswordPresenter: BasePresenter {
    var mChangePasswordDelegate : ChangePasswordDelegate?
    var request : Alamofire.Request?
    
    init(delegate : ChangePasswordDelegate) {
        super.init()
        self.mChangePasswordDelegate = delegate
    }
    
    
    func changePassword(account:String, newPass: String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mChangePasswordDelegate?.onSendingChangePassword()
            
            let changePasswordRequest : ChangePasswordRequest = ChangePasswordRequest()
            
            changePasswordRequest.login = Login()
            changePasswordRequest.login?.user = "25631"
            changePasswordRequest.login?.password = "Middle100$"
            changePasswordRequest.login?.ip = "Ip19"
            changePasswordRequest.account = account
            changePasswordRequest.newPassword = newPass
            
            let params  = Mapper().toJSON(changePasswordRequest)
            
            request = Alamofire.request(Urls.API_CHANGE_PASSWORD, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<LoginResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : LoginResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                        self.mChangePasswordDelegate?.onSuccessChangePassword(succesMessage: "Contraseña cambiada satisfactortiamente")
                    }else{
                        self.mChangePasswordDelegate?.onErrorChangePassword(errorMessage: "Ocurrio un error al cambiar la contraseña")
                    }
                case .failure(_): break
                self.mChangePasswordDelegate?.onErrorChangePassword(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mChangePasswordDelegate?.onErrorConnection()
        }
    }
}
