//
//  PaymentUniqueResponse.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 29/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import ObjectMapper

class PaymentUniqueResponse : NSObject, Mappable{
    var result : String?
    var resultDescription : String?
    var transactionNumber : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        result		<- map["Result"]
        resultDescription		<- map["ResultDescription"]
        transactionNumber		<- map["TransactionNumber"]
    }
}
