
//
//  File.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class Product : NSObject, Mappable{
    dynamic var Price_List_Name:String?
    dynamic var Description:String?
    dynamic var Precio_Impuesto:String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Price_List_Name		<- map["Price_List_Name"]
        Description		<- map["Descripcion"]
        Precio_Impuesto		<- map["Precio_Impuesto"]
    }

}
