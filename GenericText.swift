//
//  GenericItem.swift
//  MiEnlaceTP
//
//  Created by fer on 10/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class GenericText : NSObject {

    var text : String?
    
    convenience init(text : String) {
        self.init()
        self.text = text
    }
}

