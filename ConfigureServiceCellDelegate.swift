//
//  ConfigureServiceCellDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 10/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol ConfigureServicesCellDelegate: TableViewCellClickDelegate {
    
    func addServiceAditional(item : NSObject, amount : String)
    
}
