//
//  Urls.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class Urls: NSObject {
    
    static let CUSTOM_HEADERS = [
        "Content-Type": "application/json",
        "Authorization" : "Basic b2FndXNlcjpvNGd1czNy",
        "Accept" : "application/json"
    ]
    
    /// API
    
    static let API_ENLACE = "https://mss.totalplay.com.mx"
    static let API_ENLACE_TEST = "https://mss.totalplay.com.mx"
    static let API_LOGIN = API_ENLACE_TEST+"/MiEnlace/LoginUserApp"
    static let API_RECOVER = API_ENLACE_TEST+"/MiEnlace/RecoverPassword"
    static let API_REGISTER_ACCOUNT = API_ENLACE_TEST+"/MiEnlace/RegisterUser"
    static let API_GET_INFO_ACCOUNT = API_ENLACE_TEST+"/MiEnlace/GetInfoEstadoCuenta"
    static let API_SEARCH_PACKAGE = API_ENLACE_TEST+"/MiEnlace/SearchPackage"
    static let API_CHANGE_PASSWORD = API_ENLACE_TEST+"/MiEnlace/ChangePassword"
    static let API_GET_INFO_USER = API_ENLACE_TEST+"/MiEnlace/GetInfoUser"
    static let API_UPDATE_USER = API_ENLACE_TEST+"/MiEnlace/UpdateUser"
    static let API_REGISTERED_CARDS = API_ENLACE_TEST+"/MiEnlace/RegisteredCards"
    static let API_CHECK_BALANCE = API_ENLACE_TEST+"/MiEnlace/CheckBalance"
    static let API_PAYMENT_REGISTERED_CARD = API_ENLACE_TEST+"/MiEnlace/PaymentRegisteredCard"
    static let API_CHECK_BILLING_INFORMATION = API_ENLACE_TEST+"/MiEnlace/CheckBillingInformation"
    static let API_PAYMENT_UNIQUE = API_ENLACE_TEST+"/MiEnlace/PaymentUnique"
    static let API_REGISTER_CARD = API_ENLACE_TEST+"/MiEnlace/RegisterCard"
}
