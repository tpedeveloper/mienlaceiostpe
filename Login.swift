//
//  LoginRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class Login: NSObject, Mappable {
    
    var user : String?
    var password : String?
    var ip : String?
    
    override init() {
        super.init()
    }
    
    init(user: String, password: String, ip: String) {
        self.user = user
        self.password = password
        self.ip = ip
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        user <- map["User"]
        password <- map["Password"]
        ip <- map["Ip"]
    }
}
