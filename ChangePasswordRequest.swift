//
//  ChangePasswordRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangePasswordRequest : NSObject, Mappable{
    
    var login : Login?
    var account : String?
    var newPassword : String?
    
    
    override init() {
        super.init()
    }
    
    init(login: Login, account: String, newPassword: String) {
        self.login = login
        self.account=account
        self.newPassword=newPassword

    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
        account <- map["Account"]
        newPassword <- map["NewPassword"]
    }

    
}
