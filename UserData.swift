//
//  UserData.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class UserData : NSObject , Mappable{
    
    var usrId : String?
    var usrLogin : String?
    var usrEmployeeNumber : String?
    var usrFirstName : String?
    var usrLastName : String?
    var usrEmail : String?
    var usrPassword : String?
    var staId : String?
    var usrDate : String?
    var usrIdFacebook : String?
    var idDatos : String?
    var company : String?
    var area : String?
    var job : String?
    var employeesNumber : String?
    var industry : String?
    
    
    
    public override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map : Map){
        usrId <- map["USER_ID"]
        usrLogin <- map["USR_LOGIN"]
        usrEmployeeNumber <- map["USR_EMPLOYEE_NUMBER"]
        usrFirstName <- map["USR_FIRSTNAME"]
        usrLastName <- map["USR_LASTNAME"]
        usrEmail <- map["USR_EMAIL"]
        usrPassword <- map["USR_PASSWORD"]
        staId <- map["STA_ID"]
        usrDate <- map["USR_DATE"]
        usrIdFacebook <- map["USR_UD_FACEBOOK"]
        idDatos <- map["IDDATOS"]
        company <- map["EMPRESA"]
        area <- map["AREA"]
        job <- map["PUESTO"]
        employeesNumber <- map["NUM_EMPLEADOS"]
        industry <- map["SECTOR_INDUSTRIA"]
    }
    
}
