//
//  AdditionalFees.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class AdditionalFees : NSObject, Mappable{
    dynamic var LLAMADA_NACIONAL:String?
    dynamic var CELULAR_NACIONAL:String?
    dynamic var LD_INTERNACIONAL:String?
    dynamic var LD_MUNDIAL:String?
    dynamic var CUBA:String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        LLAMADA_NACIONAL		<- map["LLAMADA_NACIONAL"]
        CELULAR_NACIONAL		<- map["CELULAR_NACIONAL"]
        LD_INTERNACIONAL		<- map["LD_INTERNACIONAL"]
        LD_MUNDIAL		<- map["LD_MUNDIAL"]
        CUBA		<- map["CUBA"]
    }
    
}
