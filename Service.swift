//
//  Service.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class Service : NSObject, Mappable{
     var DN:String?
     var Service:String?
     var Productos : [Product] = []
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        DN		<- map["DN"]
        Service		<- map["Service"]
        Productos		<- map["Productos"]
    }

}
