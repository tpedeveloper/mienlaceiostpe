//
//  RealmManager.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 06/06/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//
import UIKit
import UIKit
import RealmSwift

class RealmManager: NSObject {
    
    class func mReal() -> Realm {
        return try! Realm()
    }
    
    class func findFirst<T: Object>(object : T.Type) -> T?{
        return try! Realm().objects(object).first
    }
    
    class func deleteAll<T: Object>(object: T.Type){
        let realm = try! Realm()
        try! realm.write({ () -> Void in
            realm.delete(realm.objects(object))
        })
    }
    
    class func insert<T: Object>(object: T) -> T!{
        let realm = try! Realm()
        realm.beginWrite()
        realm.add(object, update: true)
        try! realm.commitWrite()
        return object
    }
    
}

