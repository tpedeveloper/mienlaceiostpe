//
//  GetInfoUserResponse.swift
//  MiEnlaceTP
//
//  Created by fer on 02/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class GetInfoUserResponse : NSObject, Mappable{
    var Result : LoginResult?
    var userData: UserData?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Result		<- map["Result"]
        userData		<- map["UserData"]
    }
    
}

