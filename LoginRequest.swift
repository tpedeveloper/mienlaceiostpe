//
//  LoginRequest_.swift
//  MiEnlaceTP
//
//  Created by fer on 23/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//
import UIKit
import ObjectMapper

class LoginRequest: NSObject, Mappable {
    
    var login : Login?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
    }
}
