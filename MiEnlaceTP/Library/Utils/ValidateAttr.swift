//
//  ValidateAttr.swift
//  VentasTotalPlayiOS
//
//  Created by Jorge Hdez Villa on 20/01/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class ValidateAttr: NSObject {
    
    class func string(value : String?) -> String {
        return value != nil ? value!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) : ""
    }
    
    class func stringWithSecurity(value : String?) -> String {
        return value != nil ? Security.crypt(text: value!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString)  : ""
    }
    
    class func stringDescrifeSecurity(value : String?) -> String {
        return value != nil ? Security.decrypt(text: value!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))  : ""
    }
    
}
