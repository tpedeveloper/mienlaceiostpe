//
//  KeysEnum.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

//
//  Keys.swift
//  VentasTotalPlayiOS
//
//  Created by Jorge Hdez Villa on 03/02/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

extension Dictionary {
    
    subscript(key: APIKeys) -> Value? {
        get {
            return self[String(describing: key) as! Key]
        }
        set(value) {
            guard
                let value = value else {
                    self.removeValue(forKey: String(describing: key) as! Key)
                    return
            }
            
            self.updateValue(value, forKey: String(describing: key) as! Key)
        }
    }
    
}

protocol APIKeys {}

enum KeysEnum : APIKeys {
    
    case EXTRA_SALE_RELEASE
    case EXTRA_EMPLOYED
    case EXTRA_FACTIBILITY_BEAN
}

