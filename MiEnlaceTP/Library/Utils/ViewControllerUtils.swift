//
//  ViewControllerUtils.swift
//  VentasTotalPlayiOS
//
//  Created by Jorge Hdez Villa on 02/01/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class ViewControllerUtils {
    
    class func presentViewController(viewController: UIViewController, newViewController: UIViewController.Type){
        let nameController : NSString = NSStringFromClass(newViewController.classForCoder()) as NSString
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: nameController.components(separatedBy: ".").last!)
        viewController.present(vc, animated: true, completion: nil)
    }
    
    class func pushViewController(viewController: UIViewController, newViewController: UIViewController.Type){
        let nameController : NSString = NSStringFromClass(newViewController.classForCoder()) as NSString
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: nameController.components(separatedBy: ".").last!)
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    class func showDetailtViewController(viewController: UIViewController, newViewController: UIViewController.Type){
        let nameController : NSString = NSStringFromClass(newViewController.classForCoder()) as NSString
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: nameController.components(separatedBy: ".").last!)
        viewController.showDetailViewController(viewController, sender: vc)
    }
    
    class func viewController(viewController: UIViewController.Type) -> UIViewController {
        let nameController : NSString = NSStringFromClass(viewController.classForCoder()) as NSString
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: nameController.components(separatedBy: ".").last!)
        return vc
    }
    
    ///// OPEN CONTROLLER WITH EXTRAS
    
    class func presentViewController(from: UIViewController, to: UIViewController.Type, extras: [String : AnyObject]? = nil){
        let nameController : NSString = NSStringFromClass(to.classForCoder()) as NSString
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: nameController.components(separatedBy: ".").last!)
        if vc.isKind(of: BaseNavigationViewController.self) {
            let navigation : BaseNavigationViewController = vc as! BaseNavigationViewController
            if extras != nil {
                navigation.extras = extras!
            }
        } else {
            /*let resultVc = vc as! BaseViewController
            if extras != nil {
                resultVc.extras = extras!
            }*/
        }
        from.present(vc, animated: true, completion: nil)
    }

}
