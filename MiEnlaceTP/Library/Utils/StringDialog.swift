//
//  StringDialog.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public struct StringDialog {
    
    static let dialog_error_title = "Error"
    static let dialog_error_empty = "Este campo es necesario"
    static let dialig_error_all_fields_not_empty = "No deje campos vacíos"
    static let dialog_error_connection = "Revise su conexión a internet"
    static let dialog_error_server = "Ha ocurrido un error. Intente nuevamente"
    static let dialog_error_fields_empty = "Favor de ingresar lo datos solicitados"
    static let dialog_error_address = "Verifique que la direccion este bien escrita"
    static let dialog_error_version = "Existe una nueva versión de la aplicación (%@) favor de instalarla para continuar"
    static let dialog_error_user_not_exist = "Usuario no existe en Salesforce"
    static let dialog_not_videos = "No hay videos disponibles"
    
    static let dialog_error_coverage = "El servicio de cobertura no esta respondiento, intentelo mas tarde"
    
    static let dialog_error_cp = "Nuestra base de datos no cuenta con registros de este Codigo Postal"
    
    static let dialog_error_config_sale = "No ha configurado la venta"
    
    
    static let error_login = "Por favor verifica la información que proporcionaste"
    
    
    
}

