//
//  BillingInformationResponse.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class BillingInformationResponse : NSObject, Mappable{
    var result : String?
    var resultDescription : String?
    var billingInformation : BillingInformation?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        result		<- map["Result"]
        resultDescription		<- map["ResultDescription"]
        billingInformation		<- map["BillingInformation"]
    }
}
