//
//  SearchPackageDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol SearchPackageDelegate : NSObjectProtocol {
    
    func onSendingSearchPackage()
    
    func onSuccessSearchPackage(additional_fees: AdditionalFees,  arrayServPhone : [String], arrayServInter: [String], arrayDn:[String]  , plan: String, renta:String)
    
    func onErrorSearchPackage(errorMessage : String)
    
    func onErrorConnection()
    
}

