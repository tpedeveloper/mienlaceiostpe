//
//  PaymentRegisteredCardDelegate.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 31/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit


public protocol PaymentRegisteredCardDelegate: NSObjectProtocol {
    
    func onSendingPaymentRegisteredCard()
    
    func onSuccessPaymentRegisteredCard(transactionNumber: String)
    
    func onErrorPaymentRegisteredCard(errorMessage : String)
    
    func onErrorConnectionPaymentRegisteredCard(errorConnection:  String)
}
