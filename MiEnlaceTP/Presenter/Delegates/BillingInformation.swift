//
//  BillingInformation.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class BillingInformation : NSObject, Mappable{
    
     var street: String?
     var number: String?
     var colony: String?
     var zipCode: String?
     var city: String?
     var state: String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        street		<- map["Street"]
        number		<- map["Number"]
        colony		<- map["Colony"]
        zipCode		<- map["ZipCode"]
        city		<- map["City"]
        state		<- map["State"]
    }

}
