//
//  PaymentUniquePresenter.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 29/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//
import UIKit
import ObjectMapper
import Alamofire

class PaymentUniquePresenter: BasePresenter{
    
    var mPaymentUniqueDelegate : PaymentUniqueDelegate?
    var request : Alamofire.Request?
    
    init(delegate : PaymentUniqueDelegate) {
        super.init()
        self.mPaymentUniqueDelegate = delegate
    }
    
    func loadPaymentUnique(account:String, name: String,lastName:String, secondLastName:String,digitsCard:String, Cvv:String,expiryMonth:String, expiryYear:String,amount:String,codCard:String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mPaymentUniqueDelegate?.onSendingPaymentUnique()
            
            let paymentUniqueRequest : PaymentUniqueRequest = PaymentUniqueRequest()
            
            paymentUniqueRequest.login = Login()
            paymentUniqueRequest.login?.user = ValidateAttr.stringWithSecurity(value:"25631")
            paymentUniqueRequest.login?.password = ValidateAttr.stringWithSecurity(value:"Middle100$")
            paymentUniqueRequest.login?.ip = ValidateAttr.stringWithSecurity(value:"10.10.10.10")
            paymentUniqueRequest.accountNumber = ValidateAttr.stringWithSecurity(value:account)
            
            paymentUniqueRequest.newCard = NewCard();
            paymentUniqueRequest.newCard?.cardNumber = ValidateAttr.stringWithSecurity(value:digitsCard)
            paymentUniqueRequest.newCard?.amount = ValidateAttr.stringWithSecurity(value:amount)
            paymentUniqueRequest.newCard?.cvv = ValidateAttr.stringWithSecurity(value:Cvv)
            paymentUniqueRequest.newCard?.expirationMonth = ValidateAttr.stringWithSecurity(value:expiryMonth)
            paymentUniqueRequest.newCard?.expirationYear = ValidateAttr.stringWithSecurity(value:expiryYear)
            paymentUniqueRequest.newCard?.lastName = ValidateAttr.stringWithSecurity(value:lastName)
            paymentUniqueRequest.newCard?.name = ValidateAttr.stringWithSecurity(value:name)
            paymentUniqueRequest.newCard?.paymentType = ValidateAttr.stringWithSecurity(value:codCard)
            paymentUniqueRequest.newCard?.secondLastName = ValidateAttr.stringWithSecurity(value:secondLastName)
            
            
            let params  = Mapper().toJSON(paymentUniqueRequest)
            
            request = Alamofire.request(Urls.API_PAYMENT_UNIQUE, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<PaymentUniqueResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : PaymentUniqueResponse = response.result.value!
                    
                    if(objectReponse.result == "0" ){
                        self.mPaymentUniqueDelegate?.onSuccessPaymentUnique(transactionNumb: objectReponse.transactionNumber!)
                    }else{
                        self.mPaymentUniqueDelegate?.onErrorPaymentUnique(errorMessage: "¡Tu pago no ha sido autorizado!, Te recomendamos intentar el cargo con otra tarjeta de crédito o débito o realizar tu pago en cualquier institución bancaria.")
                    }
                case .failure(_): break
                self.mPaymentUniqueDelegate?.onErrorConnectionPaymentUnique(errorConnection: "No se pudo contactar al servicio, intente mas tarde")
                }
            }
        }else{
            self.mPaymentUniqueDelegate?.onErrorConnectionPaymentUnique(errorConnection: "Revise su conexión a internet")
        }
    }
}
