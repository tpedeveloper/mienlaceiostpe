//
//  GetAccountStatusDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol GetAccountStatusDelegate : NSObjectProtocol {
    
    func onSendingAccount()
    
    func onSuccessAccount(clientInfo : ClientInfo, infoAccount : AccountInfo, infoStatusAccount : AccountStatusInfo , billingHistory : [BillingHistory] , bankReferences : BankReferences )
    
    func onErrorAccount(errorMessage : String)
    
    func onErrorConnection()
    
}

