//
//  RecoverPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class RecoverPasswordPresenter: BasePresenter {
    var mRecoverDelegate : RecoverPasswordDelegate?
    var request : Alamofire.Request?
    
    init(delegate : RecoverPasswordDelegate) {
        super.init()
        self.mRecoverDelegate = delegate
    }

    
    func recover(account:String, phone: String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mRecoverDelegate?.onSendingRecover()
            
            let recoverRequest : RecoverPasswordRequest = RecoverPasswordRequest()
            
            recoverRequest.login = Login()
            recoverRequest.login?.user = "25631"
            recoverRequest.login?.password = "Middle100$"
            recoverRequest.login?.ip = "1.1.1.1"
            recoverRequest.account = account
            recoverRequest.phone = phone
            
            let params  = Mapper().toJSON(recoverRequest)
            
            request = Alamofire.request(Urls.API_RECOVER, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<LoginResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : LoginResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                        self.mRecoverDelegate?.onSuccessRecover(succesMessage:"Te enviamos una contraseña temporal al correo electrónico que tienes registrado")
                    }else{
                        self.mRecoverDelegate?.onErrorRecover(errorMessage: "El número de cuenta no es válido")
                    }
                case .failure(_): break
                self.mRecoverDelegate?.onErrorRecover(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mRecoverDelegate?.onErrorConnection()
        }
    }
}
