//
//  ServiceInternetCell.swift
//  MiEnlaceTP
//
//  Created by fer on 24/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class ServiceInternetCell: BaseTableViewCell {
    
    @IBOutlet weak var Lbl_serviceInternet: UILabel!
    var item : GenericText!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        //Configure the view for the selected state
    }
    
    override func pupulate(object :NSObject) {
        item = object as! GenericText
        Lbl_serviceInternet.text = item.text
        
    }
    
    override func toString() -> String{
        return "ServicePhoneCell"
    }
    
}

