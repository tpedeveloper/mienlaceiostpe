//
//  StateAccountResponse.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class GetAccountStatusInfoResponse  : NSObject, Mappable {
    
    
    var Result : LoginResult?
    var infoAccount :AccountInfo?
    var billingHistory : [BillingHistory] = []
    var clientInfo : ClientInfo?
    var accountStatusInfo : AccountStatusInfo?
    var bankReferences : BankReferences?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Result		<- map["Result"]
        infoAccount		<- map["InfoCuenta"]
        billingHistory		<- map["HistorialFacturacion"]
        clientInfo		<- map["InfoCliente"]
        accountStatusInfo		<- map["InfoEdoCuenta"]
        bankReferences		<- map["ReferenciasBancarias"]
    }
    
}

