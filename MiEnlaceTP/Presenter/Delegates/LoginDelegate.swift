//
//  LoginDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol LoginDelegate: NSObjectProtocol {

    func onSendingLogin()
    
    func onSuccessLogin()
    
    func onErrorLogin(errorMessage : String)
    
    func onErrorConnection()
    
}

