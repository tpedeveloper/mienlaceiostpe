//
//  BankCard.swift
//  MiEnlaceTP
//
//  Created by fer on 07/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class BankCard : NSObject {
    
    var nameCard : String?
    var numberCard : String?
    var validity : String?
    var csc : String?
    
    public override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    init(nameCard: String, numberCard: String, validity: String, csc:String) {
        self.nameCard = nameCard
        self.numberCard = numberCard
        self.validity = validity
        self.csc = validity
    }
}
