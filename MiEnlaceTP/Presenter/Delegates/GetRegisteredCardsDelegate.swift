//
//  GetRegisteredCards.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol GetRegisteredCardsDelegate: NSObjectProtocol {
    
    func onSendingRegisteredCards()
    
    func onSuccessRegisteredCards(cards: [Cards])
    
    func onErrorRegisteredCards(errorMessage : String)
    
    func onErrorConnectionRegisteredCards(errorConnection:  String)
}
