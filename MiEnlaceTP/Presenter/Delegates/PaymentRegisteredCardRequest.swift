//
//  PaymentRegisteredCardRequest.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 31/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentRegisteredCardRequest: NSObject, Mappable {
    
    var login : Login?
    var accountNumber : String?
    var idCard : String?
    var amount : String?
    var cvv: String?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map : Map){
        login <- map["Login"]
        accountNumber <- map["AccountNumber"]
        idCard <- map["IdCard"]
        amount <- map["Amount"]
        cvv <- map["Cvv"]
    }
    
}
