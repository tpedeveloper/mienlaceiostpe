//
//  dataUser.swift
//  MiEnlaceTP
//
//  Created by fer on 02/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class dataUser : NSObject , Mappable{
    
    var usrFirstName : String?
    var usrLastName : String?
    var usrEmail : String?
    var company : String?
    var area : String?
    var job : String?
    var employees : String?
    var industry : String?
    
    
    
    public override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map : Map){
        usrFirstName <- map["Name"]
        usrLastName <- map["Lastname"]
        usrEmail <- map["Email"]
        company <- map["Enterprise"]
        area <- map["Area"]
        job <- map["Job"]
        employees <- map["Employees"]
        industry <- map["SectorOrIndustry"]
    }
    
}
