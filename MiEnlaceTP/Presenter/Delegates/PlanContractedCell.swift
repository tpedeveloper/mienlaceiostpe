//
//  PlanContractedCell.swift
//  MiEnlaceTP
//
//  Created by fer on 24/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class PlanContractedCell: BaseTableViewCell {
    
    @IBOutlet weak var Lbl_date: UILabel!
    @IBOutlet weak var Lbl_total: UILabel!
    dynamic var item : BillingHistory?
    @IBOutlet weak var img_icon: UIImageView!
    
    dynamic var tapped: ((PlanContractedCell) -> Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        //Configure the view for the selected state
    }
    
    
    override func pupulate(object :NSObject) {
        item = object as! BillingHistory
        
       /* Lbl_date.text = item?.fecha!
        let saldo : Double?
        
        saldo = Double(item?.saldo!)
        Lbl_total.text =  "$\(String(format: "%.2f", saldo!))"*/
    }

    
}

