//
//  GetInfoUserPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 02/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class GetInfoUserPresenter : BasePresenter {
    
    var mGetInfoUserDelegate : GetInfoUserDelegate?
    var request : Alamofire.Request?
    
    init(delegate : GetInfoUserDelegate) {
        super.init()
        self.mGetInfoUserDelegate = delegate
    }
    
    
    func getInfoUser(account:String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mGetInfoUserDelegate?.onSendingGetInfoUser()
            
            let getInfoUserRequest : GetInfoUserRequest = GetInfoUserRequest()
            
            getInfoUserRequest.login = Login()
            getInfoUserRequest.login?.user = "25631"
            getInfoUserRequest.login?.password = "Middle100$"
            getInfoUserRequest.login?.ip = "1.1.1.1"
            getInfoUserRequest.accountNumber = account
            
            let params  = Mapper().toJSON(getInfoUserRequest)
            
            request = Alamofire.request(Urls.API_GET_INFO_USER, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<GetInfoUserResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : GetInfoUserResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                        
                        self.mGetInfoUserDelegate?.onSuccessGetInfoUser(dataUser: objectReponse.userData!)
                    }else{
                        self.mGetInfoUserDelegate?.onErrorGetInfoUser(errorMessage: "Intente mas tarde por favor")
                    }
                case .failure(_): break
                self.mGetInfoUserDelegate?.onErrorGetInfoUser(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            
        }
    }
    
}

