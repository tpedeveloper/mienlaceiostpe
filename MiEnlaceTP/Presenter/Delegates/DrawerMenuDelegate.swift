//
//  DrawerMenuDelegate.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 06/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol DrawerMenuDelegate: NSObjectProtocol {
    
    func openViewController(viewController : UIViewController)

}
