//
//  AddNewCardPresenter.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 30/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class AddNewCardPresenter : BasePresenter{
    var mAddNewCardDelegate : AddNewCardDelegate?
    var request : Alamofire.Request?
    var mCheckBalanceDelegate : CheckBalanceDelegate?
    
    init(delegate : AddNewCardDelegate) {
        super.init()
        self.mAddNewCardDelegate = delegate
    }
    
    func loadAddNewCard(account:String, name: String,lastName:String, secondLastName:String,digitsCard:String, expiryMonth:String, expiryYear:String,cctype:String){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mAddNewCardDelegate?.onSendingAddNewCard()
            
            let addNewCardRequest : AddNewCardRequest = AddNewCardRequest()
            
            addNewCardRequest.login = Login()
            addNewCardRequest.login?.user = ValidateAttr.stringWithSecurity(value:"25631")
            addNewCardRequest.login?.password = ValidateAttr.stringWithSecurity(value:"Middle100$")
            addNewCardRequest.login?.ip = ValidateAttr.stringWithSecurity(value:"10.10.10.10")
            addNewCardRequest.accountNumber = ValidateAttr.stringWithSecurity(value:account)
            
            addNewCardRequest.newCard = NewCarsAdd();
            addNewCardRequest.newCard?.cardNumber = ValidateAttr.stringWithSecurity(value:digitsCard)
            addNewCardRequest.newCard?.expirationMonth = ValidateAttr.stringWithSecurity(value:expiryMonth)
            addNewCardRequest.newCard?.expirationYear = ValidateAttr.stringWithSecurity(value:expiryYear)
            addNewCardRequest.newCard?.lastName = ValidateAttr.stringWithSecurity(value:lastName)
            addNewCardRequest.newCard?.name = ValidateAttr.stringWithSecurity(value:name)
            addNewCardRequest.newCard?.secondLastName = ValidateAttr.stringWithSecurity(value:secondLastName)
            addNewCardRequest.newCard?.cctype = ValidateAttr.stringWithSecurity(value:cctype)
            
            let params  = Mapper().toJSON(addNewCardRequest)
            
            request = Alamofire.request(Urls.API_REGISTER_CARD, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<AddNewCardResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : AddNewCardResponse  = response.result.value!
                    if(objectReponse.result == "0" ){
                        self.mAddNewCardDelegate?.onSuccessAddNewCard(msgAddNewCard: "Tarjeta agregada satisfactoriamente")
                    }else{
                        self.mAddNewCardDelegate?.onErrorAddNewCard(errorMessage: "Ocurrio un error al tratar de agregar tarjeta")
                    }
                case .failure(_): break
                self.mAddNewCardDelegate?.onErrorConnectionAddNewCard(errorConnection: "No se pudo contactar al servicio intente mas tarde")
                }
            }
        }else{
            
        }
    }
}
