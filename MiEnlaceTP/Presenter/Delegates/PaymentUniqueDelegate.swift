//
//  PaymentUniqueDelegate.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 29/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol PaymentUniqueDelegate: NSObjectProtocol {
    
    func onSendingPaymentUnique()
    
    func onSuccessPaymentUnique(transactionNumb: String)
    
    func onErrorPaymentUnique(errorMessage : String)
    
    func onErrorConnectionPaymentUnique(errorConnection:  String)
    
}
