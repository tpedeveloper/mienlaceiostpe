//
//  CheckBillingInformationDelegate.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol CheckBillingInformationDelegate: NSObjectProtocol {
    
    func onSendingCheckBillingInformation()
    
    func onSuccessCheckBillingInformation(billingInformation: BillingInformation)
    
    func onErrorCheckBillingInformation(errorMessage : String)
    
    func onErrorConnectionCheckBillingInformation(errorConnection:  String)
    
}
