//
//  DataSourceFasesAdditional.swift
//  MiEnlaceTP
//
//  Created by fer on 27/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol FasesAdditionalTableViewDelegate : NSObjectProtocol {
    func onItemClick(item: AdditionalFees)
}


class DataSourceFeesAdditional: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items : [AdditionalFees] = []
    var tableView : UITableView?
    var listadoTableViewDelegate : FasesAdditionalTableViewDelegate?
    
    init(tableView: UITableView, tableViewDelegate: FasesAdditionalTableViewDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.listadoTableViewDelegate = tableViewDelegate
    }
    
    func update(items: [AdditionalFees]) {
        self.items = items
        self.tableView?.reloadData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FeesAdditionalCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! FeesAdditionalCell
        
        let item = self.items[indexPath.row]
        cell.Lbl_feesAditional.text = item.
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        self.listadoTableViewDelegate?.onItemClick(item: item)
    }
}


