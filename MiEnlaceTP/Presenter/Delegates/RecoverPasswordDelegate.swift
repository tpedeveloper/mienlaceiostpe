//
//  RecoverDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol RecoverPasswordDelegate : NSObjectProtocol {
    
    func onSendingRecover()
    
    func onSuccessRecover(succesMessage : String)
    
    func onErrorRecover(errorMessage : String)
    
    func onErrorConnection()
    
}
