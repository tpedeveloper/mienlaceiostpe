//
//  ChangePasswordDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol ChangePasswordDelegate : NSObjectProtocol {
    
    func onSendingChangePassword()
    
    func onSuccessChangePassword(succesMessage : String)
    
    func onErrorChangePassword(errorMessage : String)
    
    func onErrorConnection()
    
}

