//
//  SelectCardResponse.swift
//  MiEnlaceTP
//
//  Created by fer on 28/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//
import UIKit
import ObjectMapper

class SelectCardResponse : NSObject, Mappable{
    var Result : LoginResult?
    dynamic var Plan:String?
    dynamic var Renta:String?
    dynamic var additionalFees: AdditionalFees?
    var services : [Service] = []
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Result		<- map["Result"]
        Plan		<- map["PLAN"]
        Renta		<- map["RENTA"]
        additionalFees		<- map["Tarifas_Adicionales_AL_Plan"]
        services		<- map["Servicios"]
    }
    
}


