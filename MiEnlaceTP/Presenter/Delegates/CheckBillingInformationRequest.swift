//
//  CheckBillingInformationRequest.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class CheckBillingInformationRequest: NSObject, Mappable {
    var login : Login?
    var accountNumber : String?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
        accountNumber <- map["AccountNumber"]
    }
}
