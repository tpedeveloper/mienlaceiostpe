//
//  SelectCardRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 28/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class SelectCardRequest: NSObject, Mappable {
    
    var login : Login?
    var account : String?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
        account <- map["Account"]
    }
}
