//
//  CheckBalanceDelegate.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol CheckBalanceDelegate: NSObjectProtocol {
    
    func onSendingCheckBalance()
    
    func onSuccessCheckBalance(balance: String)
    
    func onErrorCheckBalance(errorMessage : String)
    
    func onErrorConnectionCheckBalance(errorConnection:  String)
    
}
