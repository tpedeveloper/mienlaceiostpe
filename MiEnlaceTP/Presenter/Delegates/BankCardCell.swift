//
//  BankCardCell.swift
//  MiEnlaceTP
//
//  Created by fer on 28/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import DLRadioButton

class BankCardCell: BaseTableViewCell {
    
    @IBOutlet weak var numberCardLabel: UILabel!
    @IBOutlet weak var securityNumberTextField: UITextField!
    @IBOutlet weak var montTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var selectRadioButton: DLRadioButton!
    var item : BankCard!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //Configure the view for the selected state
    }
    
    
    override func pupulate(object :NSObject) {
        item = object as! BankCard
        
        numberCardLabel.text = item.numberCard!
        montTextField.text = item.validity!
        
        
    }
    
    override func toString() -> String{
        return "BankCardCell"
    }
    
}
