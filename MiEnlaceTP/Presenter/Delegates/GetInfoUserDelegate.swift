//
//  GetInfoUserDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 02/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol GetInfoUserDelegate : NSObjectProtocol {
    
    func onSendingGetInfoUser()
    
    func onSuccessGetInfoUser(dataUser : UserData)
    
    func onErrorGetInfoUser(errorMessage : String)
    
    func onErrorConnectionGetInfoUser()
    
}
