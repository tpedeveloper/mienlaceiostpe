//
//  AddNewCardDelegate.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 30/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol AddNewCardDelegate: NSObjectProtocol {
    
    func onSendingAddNewCard()
    
    func onSuccessAddNewCard(msgAddNewCard: String)
    
    func onErrorAddNewCard(errorMessage : String)
    
    func onErrorConnectionAddNewCard(errorConnection:  String)
}
