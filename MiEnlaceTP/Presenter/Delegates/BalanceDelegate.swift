//
//  BalanceDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 07/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol BalanceDelegate : NSObjectProtocol {
    
    func onSendingBalance()
    
    func onSuccessBalance(dataBank: [BankCard])
    
    func onErrorBalance(errorMessage : String)
    
    func onErrorConnectionBalance()
    
}
