//
//  RecoverRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class RecoverPasswordRequest: NSObject , Mappable{
    
    var account:String?
    var phone:String?
    var login: Login?

    override init() {
        super.init()
    }

    public required init?(map: Map) {
        
    }

    internal func mapping(map : Map){
        login <- map["Login"]
        account <- map["Account"]
        phone <- map["Phone"]
    }
}
