//
//  SelectCardDelegate.swift
//  MiEnlaceTP
//
//  Created by fer on 28/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol SelectCardDelegate : NSObjectProtocol {
    
    func onSendingSelectCard()
    
    func onSuccessSelectCard()
    
    func onErrorSelectCard(errorMessage : String)
    
    func onErrorConnection()
    
}

