//
//  PhoneContractedCell.swift
//  MiEnlaceTP
//
//  Created by fer on 24/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//
import UIKit

class PhoneContractedCell: BaseTableViewCell{
    @IBOutlet weak var phoneContractedLabel: UILabel!
    var item : String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        //Configure the view for the selected state
    }
    
    
    override func pupulate(object :NSObject) {
        item = object as! String
        phoneContractedLabel.text = item
    }
    
    override func toString() -> String{
        return "PhoneContractedCell"
    }


}

