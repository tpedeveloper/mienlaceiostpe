//
//  RecoverResponse.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class RecoverPasswordResponse  : NSObject, Mappable {
    
    dynamic var idResult : String?
    dynamic var result : String?
    dynamic var Description : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        idResult		<- map["IdResult"]
        result		<- map["Result"]
        Description		<- map["Description"]
    }
    
}

