//
//  AddNewCardRequest.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 30/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class AddNewCardRequest : NSObject,Mappable{
    var login : Login?
    var accountNumber : String?
    var newCard : NewCarsAdd?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map : Map){
        login <- map["Login"]
        accountNumber <- map["AccountNumber"]
        newCard <- map["NewCard"]
    }

}
