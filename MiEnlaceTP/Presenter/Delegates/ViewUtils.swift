//
//  ViewUtils.swift
//  Ventas TotalPlay
//
//  Created by Daniel García Alvarado on 07/11/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit

enum ViewVisibilityEnum{
    case GONE
    case NOT_GONE
    case VISIBLE
    case INVISIBLE
}

class ViewUtils: NSObject {

    class func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
    }
    
    class func cycleFromViewController(viewController: UIViewController,
                                 inContainerView containerView: UIView,
                                 fromOldViewController oldViewController: UIViewController,
                                 toViewController newViewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.addChildViewController(newViewController)
        self.addSubview(subView: newViewController.view, toView:containerView)
        newViewController.view.alpha = 0
        newViewController.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            newViewController.view.alpha = 1
            oldViewController.view.alpha = 0
            },
                       completion: { finished in
                        oldViewController.view.removeFromSuperview()
                        oldViewController.removeFromParentViewController()
                        newViewController.didMove(toParentViewController: viewController)
        })
    }
    
    class func setVisibility(view : UIView, visibility : ViewVisibilityEnum){
        view.clipsToBounds = true
        if visibility == ViewVisibilityEnum.INVISIBLE{
            view.isHidden = true
            view.isUserInteractionEnabled = false
        } else if visibility == ViewVisibilityEnum.VISIBLE {
            view.isHidden = false
            view.isUserInteractionEnabled = true
        } else if visibility == ViewVisibilityEnum.NOT_GONE{
            view.isHidden = false
            for constraint in view.constraints {
                if (constraint.firstAttribute == NSLayoutAttribute.height && constraint.secondAttribute == NSLayoutAttribute.notAnAttribute) {
                    if constraint.constant == 0 {
                        constraint.isActive = false
                    }
                }
            }
            
            var widthConstraint : NSLayoutConstraint?
            for constraint in view.constraints {
                if (constraint.firstAttribute == NSLayoutAttribute.width) {
                    widthConstraint = constraint;
                    break;
                }
            }
            if widthConstraint != nil{
                widthConstraint?.isActive = false
            }
            
            view.reDraw()
            
        } else if visibility == ViewVisibilityEnum.GONE {
            let constraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 0, constant: 0)
            view.addConstraint(constraint)
            let constraintWidth = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
            view.addConstraint(constraintWidth)
        }
    }
}

extension UIView {
    /// Moves the caret to the correct position by removing the trailing whitespace
    func reDraw() {
        
    }
}
