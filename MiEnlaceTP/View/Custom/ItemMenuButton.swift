//
//  ItemMenuButton.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 03/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit


@IBDesignable public class ItemMenuButton: UIControl{
    
    var view: UIView!
    var nibName = "ItemMenuButton"
    
    @IBOutlet var delegate : AnyObject!
    
    @IBInspectable var iconImage : UIImage?
    @IBInspectable var title : String?
    @IBInspectable var titleSize : Float = 12.0
    
    @IBInspectable var backgroundIcon : UIColor?
    @IBInspectable var background : UIColor?
    
    @IBOutlet weak var mContentImageView: UIView!
    @IBOutlet weak var mContentView: UIView!
    @IBOutlet weak var mIconImageView: UIImageView!
    @IBOutlet weak var mTitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.isUserInteractionEnabled = true
        xibSetup()
    }
    
    @IBAction func onTouchUpInside(_ sender: Any) {
        if delegate != nil {
            delegate?.onTouchUpInside(itemMenuButton: self)
        }
    }
    
    override public func draw(_ rect: CGRect) {
        reDraw()
    }
    
    public override func reDraw(){
        if iconImage != nil{
            mIconImageView.image = iconImage
        }
        mContentImageView.backgroundColor = backgroundIcon
        mContentView.backgroundColor = background
        mTitleLabel.text = title
        mTitleLabel.font = UIFont.systemFont(ofSize: CGFloat(titleSize))
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}

@objc public protocol ItemMenuButtonDelegate {
    
    func onTouchUpInside(itemMenuButton : ItemMenuButton)
    
}
