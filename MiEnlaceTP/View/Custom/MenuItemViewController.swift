//
//  MenuItemViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 11/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class MenuItemViewController: UIViewController {
    
    var isOpenFromMenu : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isOpenFromMenu {
            self.setNavigationBarItem()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
