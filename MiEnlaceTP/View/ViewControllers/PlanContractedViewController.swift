//
//  PlanContractedViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 09/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class PlanContractedViewController: MenuItemViewController, SearchPackageDelegate  {
    
    @IBOutlet weak var Tv_servInternet: UITableView!
    @IBOutlet weak var Tv_addtionalFees: UITableView!
    @IBOutlet weak var Tv_phoneContracted: UITableView!
    @IBOutlet weak var Tv_servPhone: UITableView!
    
    @IBOutlet weak var Lbl_rent: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var mBaseDataSourcePhoneContracted : BaseDataSource<NSObject, PhoneContractedCell>?
    var mBaseDataSourceServicePhone : BaseDataSource<NSObject, ServicePhoneCell>?
    var mBaseDataSourceInternet : BaseDataSource<NSObject, ServiceInternetCell>?
    
    var mSearchPresenter : SearchPackagePresenter?
    
    @IBOutlet weak var nationalCallLabel: UILabel!
    @IBOutlet weak var cubaLabel: UILabel!
    @IBOutlet weak var worldLdLabel: UILabel!
    @IBOutlet weak var internationlLdLabel: UILabel!
    @IBOutlet weak var nationalCelLabel: UILabel!
    
    var item : Service?
    
    @IBOutlet weak var ContractedPhoneConstraint: NSLayoutConstraint!
    @IBOutlet weak var ServPhoneConstraint: NSLayoutConstraint!
    @IBOutlet weak var ServInterConstraint: NSLayoutConstraint!
    
   
    var dataAccount: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        dataAccount = user[0].accountNumber
        
        mSearchPresenter = SearchPackagePresenter(delegate: self)
        mSearchPresenter?.searchPackage(account: dataAccount!)
        
        Tv_phoneContracted.tableFooterView = UIView()
        Tv_servPhone.tableFooterView = UIView()
        Tv_servInternet.tableFooterView = UIView()
        
        mBaseDataSourcePhoneContracted = BaseDataSource(tableView: self.Tv_phoneContracted)
        mBaseDataSourceServicePhone = BaseDataSource(tableView: self.Tv_servPhone)
        mBaseDataSourceInternet = BaseDataSource(tableView: self.Tv_servInternet)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.title = "Mi plan contratado"
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func onSendingSearchPackage() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessSearchPackage(additional_fees: AdditionalFees, arrayServPhone: [String], arrayServInter: [String], arrayDn: [String], plan: String, renta: String) {
        titleLabel.text = plan
        Lbl_rent.text = "$"+renta
        nationalCallLabel.text = additional_fees.LLAMADA_NACIONAL
        nationalCelLabel.text = additional_fees.CELULAR_NACIONAL
        internationlLdLabel.text = additional_fees.LD_INTERNACIONAL
        worldLdLabel.text = additional_fees.LD_MUNDIAL
        cubaLabel.text = additional_fees.CUBA
        
        mBaseDataSourcePhoneContracted?.update(items: arrayDn as [NSObject])
        mBaseDataSourceServicePhone?.update(items: arrayServPhone as [NSObject])
        mBaseDataSourceInternet?.update(items: arrayServInter as [NSObject])
    
        AlertDialog.hideOverlay()
        Tv_phoneContracted.layoutIfNeeded()
        ContractedPhoneConstraint.constant = Tv_phoneContracted.contentSize.height
        Tv_phoneContracted.layoutIfNeeded()
        
        Tv_servPhone.layoutIfNeeded()
        ServPhoneConstraint.constant = Tv_servPhone.contentSize.height
        Tv_servPhone.layoutIfNeeded()
        
        Tv_servInternet.layoutIfNeeded()
        ServInterConstraint.constant = Tv_servInternet.contentSize.height
        Tv_servInternet.layoutIfNeeded()
        AlertDialog.hideOverlay()
    }

    func onErrorSearchPackage(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error", body: errorMessage, view: self)
    }
    
    func onErrorConnection() {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error", body: "Verifica tu conexión", view: self)
    }
    
    public func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
       
    }

    
    @IBAction func payCardButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController:  PaymentViewController.self)
    }

}
