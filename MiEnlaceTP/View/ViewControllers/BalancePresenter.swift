//
//  BalancePresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 07/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class BalancePresenter : BasePresenter {
    var accountBank = [BankCard]()
    var mCheckBalanceDelegate : CheckBalanceDelegate?
    var request : Alamofire.Request?
    
    init(delegate : CheckBalanceDelegate) {
        super.init()
        self.mCheckBalanceDelegate = delegate
    }
    

    func getBalance(account:String){
            if ConnectionUtils.isConnectedToNetwork(){
                self.mCheckBalanceDelegate?.onSendingCheckBalance()
                let checkBalanceRequest : CheckBalanceRequest = CheckBalanceRequest()
                
                checkBalanceRequest.login = Login()
                checkBalanceRequest.login?.user = "25631"
                checkBalanceRequest.login?.password = "Middle100$"
                checkBalanceRequest.login?.ip = "10.10.10.10"
                checkBalanceRequest.accountNumber = account
                
                let params  = Mapper().toJSON(checkBalanceRequest)
                
                request = Alamofire.request(Urls.API_CHECK_BALANCE, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                    (response: DataResponse<CheckBalanceResponse>) in
                    
                    switch response.result {
                    case .success:
                        let objectReponse : CheckBalanceResponse = response.result.value!
                        if(objectReponse.balance != nil ){
                            
                            self.mCheckBalanceDelegate?.onSuccessCheckBalance(balance: objectReponse.balance!)
                            
                        }else{
                            self.mCheckBalanceDelegate?.onErrorCheckBalance(errorMessage: "Intente mas tarde por favor")
                        }
                    case .failure(_): break
                    self.mCheckBalanceDelegate?.onErrorConnectionCheckBalance(errorConnection: "No se pudo contactar al servicio intente mas tarde")
                    }
                }
                
            }else{
                
            }
    }
    
}
