//
//  ContainerViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 11/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import RealmSwift

class ContainerViewController: SlideMenuController {

    override func awakeFromNib() {
        
        let menuItemController : MenuItemViewController = ViewControllerUtils.viewController(viewController: MainViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        mainViewController = UINavigationController(rootViewController: menuItemController)
        self.mainViewController = mainViewController
        
        let letViewController : DrawerMenuViewController = ViewControllerUtils.viewController(viewController: DrawerMenuViewController.self) as! DrawerMenuViewController
        letViewController.mainViewController = mainViewController
        self.leftViewController = letViewController
        
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        let user = realm.objects(User.self)
        print("################# Hello Swift #######################",user[0].accountNumber)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
