//
//  LoginPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class LoginPresenter : BasePresenter {
    
    var mLoginDelegate : LoginDelegate?
    var request : Alamofire.Request?
    
    init(delegate : LoginDelegate) {
        super.init()
        self.mLoginDelegate = delegate
    }
    
    func login(username : String, password: String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mLoginDelegate?.onSendingLogin()
            
            let loginRequest : LoginRequest = LoginRequest()
            
            loginRequest.login = Login()//Inicializamos Constructor
            loginRequest.login?.user = username
            loginRequest.login?.password = password
            loginRequest.login?.ip = "Ip11"
            
            let params  = Mapper().toJSON(loginRequest)
            
            request = Alamofire.request(Urls.API_LOGIN, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default,headers: Urls.CUSTOM_HEADERS).responseObject {
                
                (response: DataResponse<LoginResponse>) in
                
                switch response.result {
                    
                    case .success:
                   
                        let objectReponse : LoginResponse = response.result.value!

                        if objectReponse.Result!.result == "0" {
                            self.mLoginDelegate?.onSuccessLogin()
                        } else {
                            //self.mLoginDelegate?.onSuccessLogin()
                            self.mLoginDelegate?.onErrorLogin(errorMessage: "Por favor verifica la información que proporcionaste")
                        }
                    
                    case .failure(_):
                        self.mLoginDelegate?.onErrorLogin(errorMessage: "Ocurrio un error al tratar de contactar el servidor")
                }
            }

        } else {
            self.mLoginDelegate?.onErrorConnection()
        }
    }
    
}
