//
//  PaymentUniqueViewController.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 25/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class PaymentUniqueViewController: UIViewController,CardIOPaymentViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate,PaymentUniqueDelegate, UITextFieldDelegate {
    /// This method will be called when there is a successful scan (or manual entry). You MUST dismiss paymentViewController.
    /// @param cardInfo The results of the scan.
    /// @param paymentViewController The active CardIOPaymentViewController.
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lasNameEditText: UITextField!
    @IBOutlet weak var secondLastNameEditText: UITextField!
    @IBOutlet weak var balanceLabel: UILabel!
    var yourItems=[String]()
    var pickerViewTextField : UITextField?
    @IBOutlet weak var typeCardButton: UIButton!
    var dataAmount: String?
    var mPaymentUniquePresenter : PaymentUniquePresenter?
    var monthExpry: String?
    var yearExpiry: String?
    var codCard: String = "0"
    var dataAccount : String?
    let limitLength = 3
    var indiceType : Int = 0
    
    @IBOutlet weak var contentCardView: UIView!
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            
            cardNumberLabel.text = info.cardNumber
            cvvTextField.text = info.cvv
            
            let cad = NSString(format:"%lu", info.expiryYear) as String
            let xp = cad.substring(from: 2)
            print(xp)
        
            expirationDateLabel.text = NSString(format:"%02lu/%@\n",info.expiryMonth, xp) as String
            monthExpry = NSString(format:"%02lu",info.expiryMonth) as String
            yearExpiry = NSString(format:"%lu", info.expiryYear) as String
            //resultLabel.text = str as String
        }
        
        paymentViewController?.dismiss(animated: true, completion: nil)
    }

    /// This method will be called if the user cancels the scan. You MUST dismiss paymentViewController.
    /// @param paymentViewController The active CardIOPaymentViewController.
    
    @IBAction func typeCardButton(_ sender: Any) {
         pickerViewTextField?.becomeFirstResponder()
        
        let item: String? = (yourItems[indiceType] as String)
        typeCardButton.setTitle(item, for:.normal)
        
        switch indiceType {
        case 0:
            codCard = "10003"
        case 1:
            codCard = "10005"
        default:
            codCard = "0"
        }
        
        NSLog(codCard)
    }
    
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismiss(animated: true, completion: nil)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentCardView.layer.borderWidth = 3
        self.contentCardView.layer.borderColor = UIColor.red.cgColor
        self.contentCardView.layer.cornerRadius = 8
        
        let realm = try! Realm()
        let user = realm.objects(User.self)
        cvvTextField.delegate = self
        
        dataAccount = user[0].accountNumber
        
        self.navigationItem.title = "Pago unico con tarjeta"
        
        balanceLabel.text = dataAmount!
        mPaymentUniquePresenter = PaymentUniquePresenter(delegate: self)
        
        self.navigationController?.navigationBar.topItem?.title = " "
        
        CardIOUtilities.preload()
        
        yourItems=["Crédito","Débito"]
        pickerViewTextField = UITextField(frame: CGRect.zero)
        view.addSubview(pickerViewTextField!)
        let pickerView = UIPickerView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
        pickerView.backgroundColor = .white
        
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        // set change the inputView (default is keyboard) to UIPickerView
        pickerViewTextField?.inputView = pickerView
        // add a toolbar with Cancel & Done button
        let toolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 190/255, green: 22/255, blue: 34/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PaymentUniqueViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PaymentUniqueViewController.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerViewTextField?.inputView = pickerView
        pickerViewTextField?.inputAccessoryView = toolBar


    }

    @IBAction func paymentButton(_ sender: Any) {
        if(codCard != "0"){
            if(nameTextField.text != "" && lasNameEditText.text != "" && secondLastNameEditText.text != "" && cvvTextField.text != ""){
                
                let mountDataReset = dataAmount?.substring(from: 1)
                print(mountDataReset)
                mPaymentUniquePresenter?.loadPaymentUnique(account: dataAccount!, name: nameTextField.text!, lastName: lasNameEditText.text! ,secondLastName: secondLastNameEditText.text! ,digitsCard: cardNumberLabel.text! ,Cvv: cvvTextField.text!,expiryMonth: monthExpry! ,expiryYear: yearExpiry!,amount: mountDataReset!,codCard: codCard)
            }else{
                AlertDialog.show(title: "Aviso", body: "Es necesario llenar todos los campos" , view: self)
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Es necesario seleccionar el tipo de tarjeta" , view: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func scanCardButton(_ sender: Any) {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC?.modalPresentationStyle = .formSheet
        present(cardIOVC!, animated: true, completion: nil)
    }

    func cancelTouched(_ sender: UIBarButtonItem) {
        // hide the picker view
        //pickerViewTextField?.resignFirstResponder()
    }
    
    func doneTouched(_ sender: UIBarButtonItem) {
        // hide the picker view
        //pickerViewTextField?.resignFirstResponder()
        // perform some action
    }
    
    func donePicker() {
        pickerViewTextField?.resignFirstResponder()
        NSLog("Data", "Done")
    }
    
    func cancelPicker() {
        pickerViewTextField?.resignFirstResponder()
        NSLog("Data", "Cancel")
    }
    
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yourItems.count
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item: String? = (yourItems[row] as String)
        return item!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item: String? = (yourItems[row] as String)
        indiceType = row
        typeCardButton.setTitle(item, for:.normal)
        switch row {
        case 0:
             codCard = "10003"
        case 1:
            codCard = "10005"
        default:
            codCard = "0"
        }
        NSLog(codCard)
    }

    
    func onSendingPaymentUnique() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessPaymentUnique(transactionNumb: String) {
        AlertDialog.hideOverlay()
        MainViewController.updateBalance=true
        let refreshAlert = UIAlertController(title: "CARGO EXITOSO", message: "¡Tu cargo ha sido realizado con éxito!, Autorización bancaria:" + transactionNumb, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }))
               
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func onErrorPaymentUnique(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error", body: errorMessage, view: self)
    }
    
    func onErrorConnectionPaymentUnique(errorConnection: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error de Conexión", body: errorConnection, view: self)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= limitLength
    }
    
}
