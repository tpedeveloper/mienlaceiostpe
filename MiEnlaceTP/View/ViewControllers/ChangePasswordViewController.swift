//
//  ChangePasswordViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 09/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class ChangePasswordViewController: MenuItemViewController, ChangePasswordDelegate {

    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    
    var mChangePasswordPresenter : ChangePasswordPresenter?
    var dataAccount : String?
    var mainViewController : UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewController = MainViewController()
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        dataAccount = user[0].accountNumber
        
        self.mChangePasswordPresenter = ChangePasswordPresenter(delegate: self)
       
    }

    @IBAction func savePassButton(_ sender: UIButton) {
        if((newPasswordTextfield.text?.characters.count)! > 3){
            if(newPasswordTextfield.text == confirmPasswordTextfield.text ){
                self.mChangePasswordPresenter?.changePassword(account:dataAccount! , newPass:newPasswordTextfield.text!)
            }else{
                AlertDialog.show(title: "Error", body: "Las contraseñas no coinciden", view: self)
            }
        }else{
            AlertDialog.show( title: "Error", body: "Contraseña muy corta, debe contener al menos 4 caracteres", view: self)
        }
    }
   
    @IBAction func cancelButton(_ sender: Any) {
            ViewControllerUtils.presentViewController(from: self, to: ContainerViewController.self)        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onSendingChangePassword() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessChangePassword(succesMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Aviso", body: succesMessage, view: self)
    }
    
    func onErrorChangePassword(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error", body: errorMessage, view: self)
    }
    
    func onErrorConnection() {
        AlertDialog.show(title: "Error", body: "Error en Conexión intente mas tarde", view: self)
    }

}
