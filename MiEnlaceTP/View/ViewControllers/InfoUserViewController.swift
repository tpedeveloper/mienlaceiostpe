//
//  File.swift
//  MiEnlaceTP
//
//  Created by fer on 02/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class InfoUserViewController: MenuItemViewController, GetInfoUserDelegate {
    
    @IBOutlet weak var Tf_User: UITextField!
    @IBOutlet weak var Tf_Name: UITextField!
    @IBOutlet weak var Tf_LastName: UITextField!
    @IBOutlet weak var Tf_Email: UITextField!
    @IBOutlet weak var Tf_Enterpraise: UITextField!
    @IBOutlet weak var Tf_Area: UITextField!
    @IBOutlet weak var Tf_Place: UITextField!
    @IBOutlet weak var Tf_No_Employed: UITextField!
    @IBOutlet weak var Tf_Sector: UITextField!
    var mGetInfoUserPresenter : GetInfoUserPresenter?
    
    @IBAction func btn_save(_ sender: UIButton) {
        var data : dataUser?
        data = dataUser()
        data?.usrFirstName = Tf_Name.text
        data?.usrLastName = Tf_LastName.text
        data?.usrEmail = Tf_Email.text
        data?.company = Tf_Enterpraise.text
        data?.area = Tf_Area.text
        data?.job = Tf_Place.text
        data?.employees = Tf_No_Employed.text
        data?.industry = Tf_Sector.text
    }
    
    @IBAction func btn_cancel(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mGetInfoUserPresenter = GetInfoUserPresenter(delegate: self)
        self.mGetInfoUserPresenter?.getInfoUser(account: "0200060184")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onSuccessGetInfoUser(dataUser: UserData) {
        AlertDialog.hideOverlay()
        Tf_User.text = dataUser.usrId
        Tf_Name.text = dataUser.usrFirstName
        Tf_LastName.text = dataUser.usrLastName
        Tf_Email.text = dataUser.usrEmail
        Tf_Enterpraise.text = dataUser.company
        Tf_Area.text = dataUser.area
        Tf_Place.text = dataUser.job
        Tf_No_Employed.text = dataUser.employeesNumber
        Tf_Sector.text = dataUser.industry
        
    }
    
    func onSendingGetInfoUser() {
        AlertDialog.showOverlay()
    }
    
    func onErrorConnectionGetInfoUser() {
        AlertDialog.hideOverlay()
    }
    
    func onErrorGetInfoUser(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
}
