//
//  RegisterAccountViewController.swift
//  MiEnlaceTP
//
//  Created by fer on 23/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class RegisterAccountViewController: BaseViewController, RegisterAccountDelegate {

    @IBOutlet weak var Tf_Account: UITextField!
    var mRegisterPresenter : RegisterAccountPresenter?
    
    @IBAction func Btn_register(_ sender: UIButton) {
        if(Tf_Account.text == ""){
            AlertDialog.show( title: "Aviso", body: "Favor de ingresar un número de cuenta", view: self)
        }else{
            mRegisterPresenter?.registerAccount(account: Tf_Account.text!, phone: "")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        //AlertDialog.showOverlay()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mRegisterPresenter = RegisterAccountPresenter(delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onSendingRegister() {
        AlertDialog.showOverlay()
    }
    
    func onErrorConnection() {
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error de Conexión", body: StringDialog.dialog_error_connection, view: self)
    }
    
    func onErrorRegister(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error", body: errorMessage, view: self)
    }
    
    func onSuccessRegister(succesMessage: String){
        AlertDialog.hideOverlay()
        let refreshAlert = UIAlertController(title: "Registro", message: succesMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
}
