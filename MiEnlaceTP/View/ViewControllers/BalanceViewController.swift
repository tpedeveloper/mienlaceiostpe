//
//  BalanceViewController.swift
//  MiEnlaceTP
//
//  Created by fer on 07/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class BalanceViewController : BaseViewController, BalanceDelegate, TableViewCellClickDelegate{
    var mBalancePresenter : BalancePresenter?

    //Listado History
    var mBalanceDataSource : BaseDataSource<NSObject, BankCardCell>?
    
    var balance:BankCard = BankCard()
    
    @IBOutlet weak var Tv_BankCards: UITableView!
    //Presenter
    
    override func viewDidLoad() {
        super.viewDidLoad()
       /* mBalancePresenter = BalancePresenter(delegate: self)
        Tv_BankCards.tableFooterView = UIView()
        mBalanceDataSource = BaseDataSource(tableView: self.Tv_BankCards, delegate: self)
        
        self.mBalancePresenter?.getBalanceCard(account: "0200060184")*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func onSendingBalance() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessBalance(dataBank: [BankCard]) {
        AlertDialog.hideOverlay()
        mBalanceDataSource?.update(items: dataBank)
    }
    
    func onErrorConnectionBalance() {
        AlertDialog.hideOverlay()
    }
    
    func onErrorBalance(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    func onItemClick(item: BankCard) {
        //NSWorkspace.shared().open(NSURL(string: item.link)! as URL)
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        
    }

}
