//
//  NewCard.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 29/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class NewCard : NSObject,Mappable{
    
    var name: String?
    var lastName: String?
    var secondLastName: String?
    var cardNumber: String?
    var expirationMonth: String?
    var expirationYear: String?
    var cvv: String?
    var amount: String?
    var paymentType: String?
    var cctype: String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        name		<- map["Name"]
        lastName		<- map["LastName"]
        secondLastName		<- map["SecondLastName"]
        cardNumber		<- map["CardNumber"]
        expirationMonth		<- map["ExpirationMonth"]
        expirationYear		<- map["ExpirationYear"]
        cvv		<- map["CVV"]
        amount		<- map["Amount"]
        paymentType		<- map["PaymentType"]
        cctype		<- map["CCTYPE"]
    }
}
