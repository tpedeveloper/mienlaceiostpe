//
//  AddNewCardViewController.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 30/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class AddNewCardViewController: UIViewController,AddNewCardDelegate,CardIOPaymentViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var numberCardLabel: UILabel!

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var secondLastNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    var yourItems=[String]()
    var pickerViewTextField : UITextField?
    var codCard: String = "0"
    @IBOutlet weak var typeCardButton: UIButton!
    var monthExpiry: String?
    var yearExpiry: String?
    var digitsCard: String?
    var mAddNewCardPresenter : AddNewCardPresenter?
    var dataAccount : String?
    
    var indiceType: Int = 0
    var isrefresh : Bool = false
    
    @IBOutlet weak var contentCardView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentCardView.layer.borderWidth = 3
        self.contentCardView.layer.borderColor = UIColor.red.cgColor
        self.contentCardView.layer.cornerRadius = 8
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        dataAccount = user[0].accountNumber
        
        self.navigationItem.title = "Agregar nueva tarjeta"
        
        self.navigationController?.navigationBar.topItem?.title = " "
        mAddNewCardPresenter = AddNewCardPresenter(delegate: self)
        
        CardIOUtilities.preload()
        
        yourItems=["Amex","MasterCard","Visa"]
        pickerViewTextField = UITextField(frame: CGRect.zero)
        view.addSubview(pickerViewTextField!)
        let pickerView = UIPickerView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
        pickerView.backgroundColor = .white
        
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        // set change the inputView (default is keyboard) to UIPickerView
        pickerViewTextField?.inputView = pickerView
        // add a toolbar with Cancel & Done button
        let toolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 190/255, green: 22/255, blue: 34/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddNewCardViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddNewCardViewController.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerViewTextField?.inputView = pickerView
        pickerViewTextField?.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func scanCardButton(_ sender: Any) {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC?.modalPresentationStyle = .formSheet
        present(cardIOVC!, animated: true, completion: nil)
    }
    
    @IBAction func addCardButton(_ sender: Any) {
        if(codCard == "0"){
            AlertDialog.show(title: "Advertencia", body: "Por favor seleccione el tipo de tarjeta", view: self)
        }else{
            if(nameTextfield.text != "" && lastNameTextField.text != "" && secondLastNameTextfield.text != ""){
                mAddNewCardPresenter?.loadAddNewCard(account: dataAccount!, name: nameTextfield.text!, lastName: lastNameTextField.text!, secondLastName: secondLastNameTextfield.text!, digitsCard: digitsCard!, expiryMonth: monthExpiry! , expiryYear: yearExpiry!, cctype: codCard)
            }else{
                AlertDialog.show(title: "Advertencia", body: "Es necesario llenar todos los campos", view: self)
                }
            }
    }
    
    @IBAction func typeCardButton(_ sender: Any) {
        pickerViewTextField?.becomeFirstResponder()
        let item: String? = (yourItems[indiceType] as String)
        typeCardButton.setTitle(item, for:.normal)
        var ind = indiceType+1
        codCard = String(indiceType+1)
        NSLog(codCard)
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yourItems.count
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item: String? = (yourItems[row] as String)
        return item!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item: String? = (yourItems[row] as String)
        typeCardButton.setTitle(item, for:.normal)
        codCard = item!
        indiceType = row
        var ind = indiceType+1
        codCard = String(indiceType+1)
        NSLog(codCard)
    }

    func donePicker() {
        pickerViewTextField?.resignFirstResponder()
        NSLog("Data", "Done")
    }
    
    func cancelPicker() {
        pickerViewTextField?.resignFirstResponder()
        NSLog("Data", "Cancel")
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            
            numberCardLabel.text = info.cardNumber
            
            let cad = NSString(format:"%lu", info.expiryYear) as String
            let xp = cad.substring(from: 2)
            print(xp)
            
            expirationDateLabel.text = NSString(format:"%02lu/%@\n",info.expiryMonth, xp) as String
            monthExpiry = NSString(format:"%02lu",info.expiryMonth) as String
            yearExpiry = NSString(format:"%lu", info.expiryYear) as String
            digitsCard = info.cardNumber
            //resultLabel.text = str as String
        }
        paymentViewController?.dismiss(animated: true, completion: nil)
    }

    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    func onSendingAddNewCard() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessAddNewCard(msgAddNewCard: String) {
        AlertDialog.hideOverlay()
        let refreshAlert = UIAlertController(title: "Aviso", message: msgAddNewCard, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }))
        present(refreshAlert, animated: true, completion: nil)
        isrefresh = true
    }
    
    func onErrorAddNewCard(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "", body: errorMessage, view: self)
    }
    
    func onErrorConnectionAddNewCard(errorConnection: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "", body: errorConnection, view: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if(isrefresh){
            let myVC = storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            myVC.refreshCardList()
        }
    }
}
