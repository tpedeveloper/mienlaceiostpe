//
//  UpdateProfileViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 09/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class UpdateProfileViewController: BaseViewController, GetInfoUserDelegate,UpdateUserDelegate {

    @IBOutlet weak var Tf_User: UITextField!
    @IBOutlet weak var Tf_Name: UITextField!
    @IBOutlet weak var Tf_LastName: UITextField!
    @IBOutlet weak var Tf_Email: UITextField!
    @IBOutlet weak var Tf_Enterpraise: UITextField!
    @IBOutlet weak var Tf_Area: UITextField!
    @IBOutlet weak var Tf_Place: UITextField!
    @IBOutlet weak var Tf_No_Employed: UITextField!
    @IBOutlet weak var Tf_Sector: UITextField!
    var mGetInfoUserPresenter : GetInfoUserPresenter?
    var mUpdateInfoPresenter : UpdateUserPresenter?
    var mainViewController : UIViewController!
    var dataAccount: String?
    
    
    
    @IBAction func btn_save(_ sender: UIButton) {
        var data : dataUser?
        data = dataUser()
        data?.usrFirstName = Tf_Name.text
        data?.usrLastName = Tf_LastName.text
        data?.usrEmail = Tf_Email.text
        data?.company = Tf_Enterpraise.text
        data?.area = Tf_Area.text
        data?.job = Tf_Place.text
        data?.employees = Tf_No_Employed.text
        data?.industry = Tf_Sector.text
        self.mUpdateInfoPresenter?.updateInfo(account: dataAccount!, userData: data!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewController = MainViewController()
        
        let realm = try! Realm()
        let user = realm.objects(User.self)

        dataAccount = user[0].accountNumber
        self.mGetInfoUserPresenter = GetInfoUserPresenter(delegate: self)
        self.mUpdateInfoPresenter = UpdateUserPresenter(delegate: self)
        self.mGetInfoUserPresenter?.getInfoUser(account: dataAccount!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onSuccessGetInfoUser(dataUser: UserData) {
        AlertDialog.hideOverlay()
        Tf_User.text = dataUser.usrEmployeeNumber
        Tf_Name.text = dataUser.usrFirstName
        Tf_LastName.text = dataUser.usrLastName
        Tf_Email.text = dataUser.usrEmail
        Tf_Enterpraise.text = dataUser.company
        Tf_Area.text = dataUser.area
        Tf_Place.text = dataUser.job
        Tf_No_Employed.text = dataUser.employeesNumber
        Tf_Sector.text = dataUser.industry
    }
    
    func onSendingGetInfoUser() {
        AlertDialog.showOverlay()
    }
    
    func onErrorConnectionGetInfoUser() {
        AlertDialog.hideOverlay()
    }
    
    func onErrorGetInfoUser(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    
    func onSuccessUpdateUser(successMessage: String) {
        AlertDialog.hideOverlay()
        navigationController?.popToRootViewController(animated: true)
        AlertDialog.show(title: "Aviso", body: successMessage, view: self)
    }
    
    func onErrorUpdateUser(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Aviso", body: errorMessage, view: self)
    }
    
    func onSendingUpdateUser() {
        AlertDialog.showOverlay()
    }
    
    func onErrorConnectionUpdateUser() {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Aviso", body: "Error al conectarse intente mas tarde", view: self)
    }
}
