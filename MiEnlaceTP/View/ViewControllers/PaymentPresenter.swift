//
//  PaymentPresenter.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class PaymentPresenter: BasePresenter {
    
    var mGetRegisteredCardsDelegate : GetRegisteredCardsDelegate?
    var mCheckBalanceDelegate : CheckBalanceDelegate?
    var mCheckBillingInformationDelegate : CheckBillingInformationDelegate?
    var mPaymentRegisteredCardDelegate : PaymentRegisteredCardDelegate?

    var request : Alamofire.Request?
    
    init(delegateCards : GetRegisteredCardsDelegate, delegateBilling: CheckBillingInformationDelegate, delegateBalance : CheckBalanceDelegate, delegatePaymentRegisteredCard:PaymentRegisteredCardDelegate) {
        super.init()
        self.mGetRegisteredCardsDelegate = delegateCards
        self.mCheckBalanceDelegate = delegateBalance
        self.mCheckBillingInformationDelegate = delegateBilling
        self.mPaymentRegisteredCardDelegate = delegatePaymentRegisteredCard
    }
    
    
    func getRegisteredCards(account:String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mGetRegisteredCardsDelegate?.onSendingRegisteredCards()
            let registeredCardsRequest : RegisteredCardsRequest = RegisteredCardsRequest()
            
            registeredCardsRequest.login = Login()
            registeredCardsRequest.login?.user = "25631"
            registeredCardsRequest.login?.password = "Middle100$"
            registeredCardsRequest.login?.ip = "10.10.10.10"
            registeredCardsRequest.accountNumber = account
            
            let params  = Mapper().toJSON(registeredCardsRequest)
            
            request = Alamofire.request(Urls.API_REGISTERED_CARDS, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<RegisteredCardsResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : RegisteredCardsResponse = response.result.value!
                    if(objectReponse.result == "0" ){
                        
                        self.mGetRegisteredCardsDelegate?.onSuccessRegisteredCards(cards: objectReponse.cards)
                    }else{
                        self.mGetRegisteredCardsDelegate?.onErrorConnectionRegisteredCards(errorConnection: "Intente mas tarde por favor")
                    }
                case .failure(_): break
                self.mGetRegisteredCardsDelegate?.onErrorRegisteredCards(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mGetRegisteredCardsDelegate?.onErrorConnectionRegisteredCards(errorConnection: StringDialog.dialog_error_connection)
        }
    }
    
    
    
    func getBalance(account:String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mCheckBalanceDelegate?.onSendingCheckBalance()
            let checkBalanceRequest : CheckBalanceRequest = CheckBalanceRequest()
            
            checkBalanceRequest.login = Login()
            checkBalanceRequest.login?.user = "25631"
            checkBalanceRequest.login?.password = "Middle100$"
            checkBalanceRequest.login?.ip = "10.10.10.10"
            checkBalanceRequest.accountNumber = account
            
            let params  = Mapper().toJSON(checkBalanceRequest)
            
            request = Alamofire.request(Urls.API_CHECK_BALANCE, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<CheckBalanceResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : CheckBalanceResponse = response.result.value!
                    if(objectReponse.balance != nil ){
                        
                        self.mCheckBalanceDelegate?.onSuccessCheckBalance(balance: objectReponse.balance!)
                        
                    }else{
                        self.mCheckBalanceDelegate?.onErrorCheckBalance(errorMessage: "Intente mas tarde por favor")
                    }
                case .failure(_): break
                self.mGetRegisteredCardsDelegate?.onErrorRegisteredCards(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mGetRegisteredCardsDelegate?.onErrorConnectionRegisteredCards(errorConnection: StringDialog.dialog_error_connection)
        }
    }
    
    
    func getBillingInformation(account:String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mCheckBillingInformationDelegate?.onSendingCheckBillingInformation()
            let billinInformationRequest : CheckBillingInformationRequest = CheckBillingInformationRequest()
            
            billinInformationRequest.login = Login()
            billinInformationRequest.login?.user = "25631"
            billinInformationRequest.login?.password = "Middle100$"
            billinInformationRequest.login?.ip = "10.10.10.10"
            billinInformationRequest.accountNumber = account
            
            let params  = Mapper().toJSON(billinInformationRequest)
            
            request = Alamofire.request(Urls.API_CHECK_BILLING_INFORMATION, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<BillingInformationResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : BillingInformationResponse = response.result.value!
                    if(objectReponse.result == "0" ){
                        
                        self.mCheckBillingInformationDelegate?.onSuccessCheckBillingInformation(billingInformation: objectReponse.billingInformation!)
                    }else{
                        self.mCheckBillingInformationDelegate?.onErrorCheckBillingInformation(errorMessage: "Intente mas tarde por favor")
                    }
                case .failure(_): break
                self.mCheckBillingInformationDelegate?.onErrorConnectionCheckBillingInformation(errorConnection: "No se pudo contactar al servicio intente mas tarde")
                }
            }
        }else{
            self.mCheckBillingInformationDelegate?.onErrorConnectionCheckBillingInformation(errorConnection: StringDialog.dialog_error_connection)
        }
    }
    
    
    func paymentRegisteredCard(account: String, numberCard: String , cvv: String, amount: String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mPaymentRegisteredCardDelegate?.onSendingPaymentRegisteredCard()
            let paymentRegisteredCardRequest : PaymentRegisteredCardRequest = PaymentRegisteredCardRequest()
            
            paymentRegisteredCardRequest.login = Login()
            paymentRegisteredCardRequest.login?.user = "25631"
            paymentRegisteredCardRequest.login?.password = "Middle100$"
            paymentRegisteredCardRequest.login?.ip = "10.10.10.10"
            paymentRegisteredCardRequest.accountNumber = account
            paymentRegisteredCardRequest.amount = amount
            paymentRegisteredCardRequest.cvv = ValidateAttr.stringWithSecurity(value:cvv)
            paymentRegisteredCardRequest.idCard = numberCard
            
            
            let params  = Mapper().toJSON(paymentRegisteredCardRequest)
            
            request = Alamofire.request(Urls.API_PAYMENT_REGISTERED_CARD, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<PaymentRegisteredCard>) in
                
                switch response.result {
                case .success:
                    let objectReponse : PaymentRegisteredCard = response.result.value!
                    if(objectReponse.result == "0" ){
                       self.mPaymentRegisteredCardDelegate?.onSuccessPaymentRegisteredCard(transactionNumber: objectReponse.transactionNumber!)
                    }else{
                        self.mPaymentRegisteredCardDelegate?.onErrorPaymentRegisteredCard(errorMessage: "¡Tu pago no ha sido autorizado!, Te recomendamos intentar el cargo con otra tarjeta de crédito o débito o realizar tu pago en cualquier institución bancaria.")
                    }
                case .failure(_): break
                    self.mPaymentRegisteredCardDelegate?.onErrorConnectionPaymentRegisteredCard(errorConnection: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mPaymentRegisteredCardDelegate?.onErrorConnectionPaymentRegisteredCard(errorConnection: StringDialog.dialog_error_connection)
        }
    }
    
    
    
}
