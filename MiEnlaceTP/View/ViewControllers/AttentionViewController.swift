//
//  AttentionViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 09/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class AttentionViewController: MenuItemViewController {
    @IBAction func btn_call(_ sender: UIButton) {
        callNumber(phoneNumber: "018003033333")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func callNumber(phoneNumber:String) {
        guard let url = URL(string: "telprompt://" + phoneNumber) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
