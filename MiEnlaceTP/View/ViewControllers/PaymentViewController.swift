//
//  PaymentViewController.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class PaymentViewController: MenuItemViewController, iCarouselDataSource, iCarouselDelegate,CheckBalanceDelegate,CheckBillingInformationDelegate,GetRegisteredCardsDelegate, PaymentRegisteredCardDelegate,UITextFieldDelegate {
    
    var cardsArray = [Cards]()
    var selectedIndex : Int!
    @IBOutlet var vwCarousel: iCarousel!
    var mPaymentPresenter : PaymentPresenter?
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var colonyLabel: UILabel!
    @IBOutlet weak var zipCodeLabel: UILabel!
    @IBOutlet weak var delegationLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    
    static var viewCarousel:iCarousel?
    
    var numbCard: String?
    static var dataAccount : String?
    let limitLength = 3
    var indice : Int = 0
    var cardNumbFourDigits : String?
    static var updateBalance : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PaymentViewController.viewCarousel = vwCarousel
        
        self.navigationItem.title = "Administrar tarjetas"
        self.navigationController?.navigationBar.topItem?.title = " "
        
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        PaymentViewController.dataAccount = user[0].accountNumber
        
        let shareBtnIcon = UIImage(named: "ic_add")
        let shareButton = UIButton(type: .custom)
        
        shareButton.frame = CGRect(x: CGFloat(20), y: CGFloat(10), width: CGFloat(30), height: CGFloat(30))
        shareButton.setBackgroundImage(shareBtnIcon, for: .normal)
        shareButton.addTarget(self, action: #selector(self.addNewCardClick), for: .touchUpInside)
        //Create BarButton using ImageButton
        let shareBarBtn = UIBarButtonItem(customView: shareButton)
        //Add BarButton to NavigationBar
        navigationItem.rightBarButtonItems = [shareBarBtn]
        
        mPaymentPresenter = PaymentPresenter(delegateCards: self, delegateBilling: self, delegateBalance: self, delegatePaymentRegisteredCard: self)
        
        mPaymentPresenter?.getBalance(account: PaymentViewController.dataAccount!)
        mPaymentPresenter?.getBillingInformation(account: PaymentViewController.dataAccount!)
        mPaymentPresenter?.getRegisteredCards(account: PaymentViewController.dataAccount!)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.title = "Administrar tarjetas"
        mPaymentPresenter?.getBalance(account: PaymentViewController.dataAccount!)
        print("Udate Balance")
   }
    
    func addNewCardClick(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: AddNewCardViewController? = storyboard.instantiateViewController(withIdentifier: "AddNewCardViewController") as! AddNewCardViewController
        navigationController?.pushViewController(controller!, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of an y resources that can be recreated.
    }
    
    //MARK: icarousel delegate methods
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return cardsArray.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view = Bundle.main.loadNibNamed("CellCardCollectionReusableView", owner: self, options: nil)![0] as! CellCardCollectionReusableView
        /*view.frame.size = CGSize(width: self.view.frame.size.width/2, height: 83.0)
        view.backgroundColor = UIColor.lightGray*/
        cardNumbFourDigits = cardsArray[index].cardNumber
        view.fourDigitsLabel.text = cardNumbFourDigits?.substring(from: 1)
        view.ownerLabel.text = cardsArray[index].name
        let month : String = cardsArray[index].expirationMonth!
        let year : String = cardsArray[index].expirationYear!
        
        view.expirationDateLabel.text = month == "" ? " " : "\(month)/\(year)"
        
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        selectedIndex = index
        //self .performSegue(withIdentifier: "imageDisplaySegue", sender: nil)
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        indice = carousel.currentItemIndex
        
        do {
            if(carousel.currentItemIndex == 0){
                numbCard = cardsArray[0].cardNumber
                print("Numero de Tarjeta", numbCard!)
            }else{
                numbCard = cardsArray[carousel.currentItemIndex].cardNumber
                print("Numero de Tarjeta", numbCard!)
            }
            
        } catch let error as String{
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        /*if (segue.identifier == "imageDisplaySegue")
        {
            var viewController : ImageDisplayViewController = segue.destination as! ImageDisplayViewController
            viewController.selectedImage = UIImage(named: "\(images.object(at: selectedIndex))")
        }*/
    }
    
    @IBAction func paymentUniqueButton(_ sender: Any) {
        let myVC = storyboard?.instantiateViewController(withIdentifier: "PaymentUniqueViewController") as! PaymentUniqueViewController
        myVC.dataAmount = balanceLabel.text
        navigationController?.pushViewController(myVC, animated: true)
    }

    @IBAction func paymentButton(_ sender: Any) {
        if(cardsArray.count>1){
            if(indice > 0){
                self.numbCard = cardsArray[indice].cardNumber
                print("Numero 1",self.numbCard)
            }else{
                self.numbCard = cardsArray[0].cardNumber
                print("Numero 0",self.numbCard)
            }
            
            
            let alertController = UIAlertController(title: "Ingresar CVV", message: "", preferredStyle: .alert)
        
            let saveAction = UIAlertAction(title: "Pagar", style: .default, handler: {
                alert -> Void in
            
                let cvvTextField = alertController.textFields![0] as UITextField
            
                self.mPaymentPresenter?.paymentRegisteredCard(account: PaymentViewController.dataAccount!, numberCard: self.numbCard!, cvv: cvvTextField.text!, amount: self.balanceLabel.text!)
            //print("firstName \(firstTextField.text), secondName \(secondTextField.text)")
            
            })
        
            let cancelAction = UIAlertAction(title: "Cancelar", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            
            })
        
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "CVV"
                textField.isSecureTextEntry  = true
                textField.delegate = self
            }
        
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
        
            self.present(alertController, animated: true, completion: nil)
        }else{
            AlertDialog.show(title: "Aviso", body: "Es ncesario tener al menos una tarjeta registrada", view: self)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= limitLength
    }
    
    func onSendingCheckBillingInformation() {
        
    }
    
    func onSendingCheckBalance() {
        
    }
    
    func onSendingRegisteredCards() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessCheckBalance(balance: String) {
        let morePrecisePI = Double(balance)
        let roundedF = CGFloat(ceil(Double(morePrecisePI!)))
        
        balanceLabel.text = "$\(String(format: "%.1f", roundedF))"
    }
    
    func randomCGFloat() -> Float {
        return Float(arc4random()) /  Float(UInt32.max)
    }
    
    func onSuccessCheckBillingInformation(billingInformation: BillingInformation) {        
        addressLabel.text = billingInformation.street
        colonyLabel.text = billingInformation.colony
        zipCodeLabel.text = billingInformation.zipCode
        delegationLabel.text = billingInformation.city
        stateLabel.text = billingInformation.state
    }
    
    func onSuccessRegisteredCards(cards: [Cards]) {
        cardsArray = cards
        vwCarousel = PaymentViewController.viewCarousel
        vwCarousel.type = iCarouselType.coverFlow2
        vwCarousel.delegate = self
        vwCarousel.dataSource = self
        vwCarousel .reloadData()
        AlertDialog.hideOverlay()
    }
    
    func onErrorCheckBalance(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    func onErrorRegisteredCards(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    func onErrorCheckBillingInformation(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    func onErrorConnectionCheckBalance(errorConnection: String) {
        AlertDialog.hideOverlay()
    }
    
    
    func onErrorConnectionRegisteredCards(errorConnection: String) {
        let c : Cards = Cards()
        c.cardNumber = ""
        c.expirationMonth = ""
        c.expirationYear = ""
        c.name = ""
        c.idCard = ""
        
        cardsArray.append(c)
        vwCarousel = PaymentViewController.viewCarousel
        vwCarousel.type = iCarouselType.coverFlow2
        vwCarousel.delegate = self
        vwCarousel.dataSource = self
        vwCarousel .reloadData()
        AlertDialog.hideOverlay()
    }
    
    
    func onErrorConnectionCheckBillingInformation(errorConnection: String) {
        AlertDialog.hideOverlay()
    }
    
    
    
    func onSendingPaymentRegisteredCard() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessPaymentRegisteredCard(transactionNumber: String) {
        AlertDialog.hideOverlay()
        
        MainViewController.updateBalance=true
        
        let refreshAlert = UIAlertController(title: "CARGO EXITOSO", message: "¡Tu cargo ha sido realizado con éxito! Autorización bancaria:" + transactionNumber, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            /*self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)*/
            self.mPaymentPresenter?.getBalance(account: PaymentViewController.dataAccount!)
            self.mPaymentPresenter?.getBillingInformation(account: PaymentViewController.dataAccount!)
            self.mPaymentPresenter?.getRegisteredCards(account: PaymentViewController.dataAccount!)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func onErrorPaymentRegisteredCard(errorMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error", body: errorMessage, view: self)
    }
    
    func onErrorConnectionPaymentRegisteredCard(errorConnection: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show(title: "Error", body:errorConnection, view: self)
    }
    
    func refreshCardList(){
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        PaymentViewController.dataAccount = user[0].accountNumber
        
         mPaymentPresenter = PaymentPresenter(delegateCards: self, delegateBilling: self, delegateBalance: self, delegatePaymentRegisteredCard: self)
        mPaymentPresenter?.getRegisteredCards(account: PaymentViewController.dataAccount!)
    }
}
