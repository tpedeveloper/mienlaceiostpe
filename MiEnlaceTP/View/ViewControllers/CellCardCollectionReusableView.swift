//
//  CellCollectionReusableView.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 22/05/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class CellCardCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var fourDigitsLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 3
        self.layer.cornerRadius = 8
    }
    
}
