//
//  DrawerMenuViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 06/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
class DrawerMenuViewController: UIViewController {
    
    @IBOutlet weak var mHomeButton: ItemMenuButton!
    @IBOutlet weak var mProfileButton: ItemMenuButton!
    @IBOutlet weak var mUpdateProfileSubMenuButton: ItemMenuButton!
    @IBOutlet weak var mChangePasswordSubMenuButton: ItemMenuButton!
    @IBOutlet weak var mStateAccountButton: ItemMenuButton!
    @IBOutlet weak var mStateAccountSubMenuButton: ItemMenuButton!
    //@IBOutlet weak var mDetailCallsSubMenuButton: ItemMenuButton!
    @IBOutlet weak var mAdministratorTDCSubMenuButton: ItemMenuButton!
    @IBOutlet weak var mPlanButton: ItemMenuButton!
    @IBOutlet weak var mAttentionButton: ItemMenuButton!
    @IBOutlet weak var mLogoutButton: ItemMenuButton!
    
    @IBOutlet weak var plainLabel: UILabel!
    @IBOutlet weak var nameCustomerLabel: UILabel!
    
    static var plain : UILabel?
    static var nameClient: UILabel?
    
    var isGoneProfile : Bool = false
    var isGoneStateAccount : Bool = false
    
    var mainViewController : UIViewController!
    var updateProfileViewController : UIViewController!
    var updatePasswordViewController : UIViewController!
    var stateAccountViewController : UIViewController!
    var detailCallsViewController : UIViewController!
    var administratorTDCViewController : UIViewController!
    var planViewController : UIViewController!
    var attentionViewController : UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewController = MainViewController()
        
        DrawerMenuViewController.nameClient = nameCustomerLabel
        DrawerMenuViewController.plain = plainLabel
        
        ViewUtils.setVisibility(view: mUpdateProfileSubMenuButton, visibility: ViewVisibilityEnum.GONE)
        ViewUtils.setVisibility(view: mChangePasswordSubMenuButton, visibility: ViewVisibilityEnum.GONE)
        
        ViewUtils.setVisibility(view: mStateAccountSubMenuButton, visibility: ViewVisibilityEnum.GONE)
        /*ViewUtils.setVisibility(view: mDetailCallsSubMenuButton, visibility: ViewVisibilityEnum.GONE)*/
        ViewUtils.setVisibility(view: mAdministratorTDCSubMenuButton, visibility: ViewVisibilityEnum.GONE)
        
        var menuItemController : MenuItemViewController = ViewControllerUtils.viewController(viewController: MainViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        mainViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: EditViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        updateProfileViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: ChangePasswordViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        updatePasswordViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: StateAccountViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        stateAccountViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: DetailCallsViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        detailCallsViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: PaymentViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        administratorTDCViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: PlanContractedViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        planViewController = UINavigationController(rootViewController: menuItemController)
        
        menuItemController = ViewControllerUtils.viewController(viewController: AttentionViewController.self) as! MenuItemViewController
        menuItemController.isOpenFromMenu = true
        attentionViewController = UINavigationController(rootViewController: menuItemController)
    
    }
    
    func onTouchUpInside(itemMenuButton : ItemMenuButton){
        switch itemMenuButton {
        case mHomeButton:
            self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
            break
        case mAttentionButton:
            self.slideMenuController()?.changeMainViewController(attentionViewController, close: true)
            attentionViewController.setNavigationBarItem()
            break
        case mUpdateProfileSubMenuButton:
            self.slideMenuController()?.changeMainViewController(updateProfileViewController, close: true)
            break
        case mChangePasswordSubMenuButton:
            self.slideMenuController()?.changeMainViewController(updatePasswordViewController, close: true)
            break
        case mPlanButton:
            self.slideMenuController()?.changeMainViewController(planViewController, close: true)
            break;
        /*case mDetailCallsSubMenuButton:
            self.slideMenuController()?.changeMainViewController(detailCallsViewController, close: true)
            break;*/
        case mAdministratorTDCSubMenuButton:
            self.slideMenuController()?.changeMainViewController(administratorTDCViewController, close: true)
            break;
        case mStateAccountSubMenuButton:
            self.slideMenuController()?.changeMainViewController(stateAccountViewController, close: true)
            break;
        case mStateAccountButton:
            var visibility = ViewVisibilityEnum.GONE
            if !isGoneStateAccount {
                visibility = ViewVisibilityEnum.NOT_GONE
            }
            isGoneStateAccount = !isGoneStateAccount
            ViewUtils.setVisibility(view: mStateAccountSubMenuButton, visibility: visibility)
            //ViewUtils.setVisibility(view: mDetailCallsSubMenuButton, visibility: visibility)
            ViewUtils.setVisibility(view: mAdministratorTDCSubMenuButton, visibility: visibility)
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            break;
        case mProfileButton:
            var visibility = ViewVisibilityEnum.GONE
            if !isGoneProfile {
                visibility = ViewVisibilityEnum.NOT_GONE
            }
            isGoneProfile = !isGoneProfile
            ViewUtils.setVisibility(view: mUpdateProfileSubMenuButton, visibility: visibility)
            ViewUtils.setVisibility(view: mChangePasswordSubMenuButton, visibility: visibility)
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            break;
        case mLogoutButton:
            
            self.dismiss(animated: true, completion: nil)
            break;
        default:
            print("")
        }
    }
    
    func showAttentionViewController(){
        self.slideMenuController()?.changeMainViewController(attentionViewController, close: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func addNameCustomer(name : String, plain:String){
        nameCustomerLabel = DrawerMenuViewController.nameClient
        plainLabel = DrawerMenuViewController.plain
        
        nameCustomerLabel.text = name
        plainLabel.text = plain
    }
}
