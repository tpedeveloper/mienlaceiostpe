//
//  StateAccountViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 10/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}

class StateAccountViewController: MenuItemViewController,GetAccountStatusDelegate,TableViewCellClickDelegate {

    //Bind mi cuenta
    @IBOutlet weak var Img_user: UIImageView!
    @IBOutlet weak var Lbl_name: UILabel!
    @IBOutlet weak var Lbl_No_Account: UILabel!
    @IBOutlet weak var Lbl_Phone: UILabel!
    
    @IBOutlet weak var Tv_HistorialTable: UITableView!
    @IBOutlet weak var ivaLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var chargesMonthLabel: UILabel!
    
    //Bind A pagar
    @IBOutlet weak var Lbl_Pay_Message: UILabel!
    
    @IBOutlet weak var Lbl_Pay_Type: UIView!
    @IBOutlet weak var Lbl_Pay1: UILabel!
    @IBOutlet weak var Lbl_Pay_Message2: UILabel!
    @IBOutlet weak var Lbl_Pay2: UILabel!
    @IBOutlet weak var Lbl_Pay_Type2: UILabel!
    
    
    //Bind Apartado Estado de cuenta
    @IBOutlet weak var Lbl_Before_Balance: UILabel!
    @IBOutlet weak var Lbl_discount: UILabel!
    @IBOutlet weak var Lbl_modify: UILabel!
    @IBOutlet weak var Lbl_Pay_Table: UILabel!
    
    //Bind Cargos del mes
    @IBOutlet weak var Lbl_rent_plan: UILabel!
    @IBOutlet weak var Lbl_extras_services: UILabel!
    @IBOutlet weak var Lbl_charges: UILabel!
    @IBOutlet weak var Lbl_sub: UILabel!
    @IBOutlet weak var Lbl_iva: UILabel!
    @IBOutlet weak var Lbl_month_charges: UILabel!
    
    //Bind table
    
    @IBOutlet weak var Lbl_before_balance: UILabel!
    @IBOutlet weak var Lbl_discount_point_pay: UILabel!
   
    @IBOutlet weak var Lbl_otherCharge: UILabel!
    
    @IBOutlet weak var Lbl_pay_table: UILabel!
    
    @IBOutlet weak var Lbl_month_before_sub: UILabel!
  
    @IBOutlet weak var Lbl_consumtions: UILabel!
    @IBOutlet weak var Lbl_bonuses: UILabel!
    
    @IBOutlet weak var C_TableHeight: NSLayoutConstraint!
    
    //Listado History
    var mhistorailDataSource : BaseDataSource<NSObject, HistorialListadoCell>?
    
    var bHistory:BillingHistory = BillingHistory()
    
    //Presenter
    var mGetAccountPresenter : GetAccountStatusPresenter?
    
    var dataAccount : String?
    
    var otherCharges : Double?
    var balanceAgo : Double?
    var discountPay : Double?
    var pay : Double?
    var planRent : Double?
    var costumer : Double?
    var extrasServices : Double?
    
    //bind Reference BankReferences
    
    @IBOutlet weak var speiBankLabel: UILabel!
    @IBOutlet weak var bankAztecaLabel: UILabel!
    @IBOutlet weak var bancomerLabel: UILabel!
    @IBOutlet weak var banamexLabel: UILabel!
    @IBOutlet weak var santanderLabel: UILabel!
    @IBOutlet weak var scotiaBankLabel: UILabel!
    @IBOutlet weak var banorteLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        AlertDialog.showOverlay()
        
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        dataAccount = user[0].accountNumber
        
        Tv_HistorialTable.tableFooterView = UIView()
        mhistorailDataSource = BaseDataSource(tableView: self.Tv_HistorialTable, delegate: self)
        
        mGetAccountPresenter = GetAccountStatusPresenter(delegate: self)
        mGetAccountPresenter?.stateAccount(No_Account: dataAccount!)

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.title = "Mi estado de cuenta"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    } 
    
    func onSendingAccount() { 
        AlertDialog.showOverlay()
    }
    
    func onErrorConnection() {
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error", body: StringDialog.dialog_error_connection, view: self)
    }
    
    func onSuccessAccount(clientInfo: ClientInfo, infoAccount: AccountInfo, infoStatusAccount: AccountStatusInfo, billingHistory: [BillingHistory], bankReferences: BankReferences) {
        AlertDialog.hideOverlay()
        
        
        Lbl_name.text = clientInfo.Nombre
        Lbl_Phone.text = infoAccount.Dn
        Lbl_No_Account.text = infoAccount.NoCuenta
        
        Lbl_Pay_Message.text = infoStatusAccount.FechaPP == nil ? "Antes de" : "Antes de "+infoStatusAccount.FechaPP!
        
        
        Lbl_Pay1.text = infoStatusAccount.PrecioPP == nil ? " " : "$\(infoStatusAccount.PrecioPP!)"
        
        Lbl_Pay_Type2.text = "Pronto Pago"
        
        Lbl_Pay_Message2.text = infoStatusAccount.FechaL == nil ? " " : "Antes de "+infoStatusAccount.FechaL!
        Lbl_Pay2.text = infoStatusAccount.PrecioL == nil ? " " : "$\(infoStatusAccount.PrecioL!)"
        Lbl_Pay_Type2.text = "Precio de Lista"
        
        balanceAgo = infoStatusAccount.SaldoAnterior == nil ? 0.0 : Double(infoStatusAccount.SaldoAnterior!)
        Lbl_before_balance.text = "$\(String(format: "%.2f", balanceAgo!))" //infoStatusAccount.SaldoAnterior == nil ? "" : "$\(infoStatusAccount.SaldoAnterior!)"
        
        discountPay = infoStatusAccount.DescuentoPP == nil ? 0.0:  Double(infoStatusAccount.DescuentoPP!)
        Lbl_discount_point_pay.text = "$\(String(format: "%.2f", discountPay!))"
        
        otherCharges = infoStatusAccount.OtrosCargosBonificaciones == nil ? 0.0 :  Double(infoStatusAccount.OtrosCargosBonificaciones!)
        Lbl_otherCharge.text =  "$\(String(format: "%.2f", otherCharges!))"//infoStatusAccount.OtrosCargosBonificaciones == nil ? " " :  "$\(infoStatusAccount.OtrosCargosBonificaciones!)"
        
        Lbl_pay_table.text = infoStatusAccount.Pago == nil ? " " : "$\(infoStatusAccount.Pago!)"
        
        let balanceMonthBefore : Double?
        
        balanceMonthBefore = infoStatusAccount.SubTotalMesAnterior == nil ? 0.0 : Double(infoStatusAccount.SubTotalMesAnterior!)

        Lbl_month_before_sub.text =  "$\(String(format: "%.2f", balanceMonthBefore!))"
        
        planRent = infoStatusAccount.RentaPlan == nil ? 0.0 : Double(infoStatusAccount.RentaPlan!)
        Lbl_rent_plan.text = "$\(String(format: "%.2f", planRent!))"//infoStatusAccount.RentaPlan == nil ? " " : "$\(infoStatusAccount.RentaPlan!)"
        
        costumer = infoStatusAccount.Consumos == nil ? 0.0 : Double(infoStatusAccount.Consumos!)
        Lbl_consumtions.text = "$\(String(format: "%.2f", costumer!))"//infoStatusAccount.Consumos == nil ? " " : "$\(infoStatusAccount.Consumos!)"
        
        extrasServices = infoStatusAccount.ServExtras == nil ? 0.0 : Double(infoStatusAccount.ServExtras!)
        Lbl_extras_services.text = "$\(String(format: "%.2f", extrasServices!))"///infoStatusAccount.ServExtras == nil ? " " : "$\(infoStatusAccount.ServExtras!)"
        
        Lbl_bonuses.text = infoStatusAccount.Bonificaciones == nil ? " " : "$\(infoStatusAccount.Bonificaciones!)"
        Lbl_charges.text = "$0"

        
        subTotalLabel.text = infoStatusAccount.SubTotalMes == nil ? " " : "$\(infoStatusAccount.SubTotalMes!)"
        chargesMonthLabel.text = infoStatusAccount.SaldoTotalMes == nil ? "" :"$\(infoStatusAccount.SaldoTotalMes!)"
        
        let myInt = infoStatusAccount.SubTotalMes == nil ? 0.0 : Double(infoStatusAccount.SubTotalMes!)
        
        let subtotal = myInt! * 0.16
        
        ivaLabel.text = "$\(String(format: "%.2f", subtotal))"
        
        
        mhistorailDataSource?.update(items: billingHistory)
        
        Tv_HistorialTable.layoutIfNeeded()
        C_TableHeight.constant = Tv_HistorialTable.contentSize.height
        Tv_HistorialTable.layoutIfNeeded()
        bankAztecaLabel.text = bankReferences.BancoAzteca == nil ? "" : trimBlankSpace(cadena:bankReferences.BancoAzteca!)
        banamexLabel.text = bankReferences.Banamex == nil ? "" : trimBlankSpace(cadena:bankReferences.Banamex!)
        banorteLabel.text = bankReferences.Banorte == nil ? "" : trimBlankSpace(cadena:bankReferences.Banorte!)
        santanderLabel.text = bankReferences.Santander == nil ? "" : trimBlankSpace(cadena:bankReferences.Santander!)
        bancomerLabel.text = bankReferences.Bancomer == nil ? " " : trimBlankSpace(cadena: bankReferences.Bancomer!)
        scotiaBankLabel.text = bankReferences.Scotianbank == nil ? " " : trimBlankSpace(cadena:bankReferences.Scotianbank!)
        speiBankLabel.text = bankReferences.Spei == nil ? "": "SPEI "+bankReferences.Spei!+" de Banco Azteca"
    }
    
    func  trimBlankSpace(cadena: String) -> String {
        
        var i: Int = 0
        var aux: String = ""
        var cha: Character?
        var letter: String = ""
        
        while i < cadena.characters.count{
            cha = cadena[cadena.index(cadena.startIndex, offsetBy: i)]
            
            if(cha == " "){
                
            }else{
                letter = String(cha!)
                aux=aux+letter
            }
            
            i += 1
        }
        
        return aux
    }
    
    
    func onErrorAccount(errorMessage: String) {
        AlertDialog.hideOverlay()
        
        if errorMessage == "" {
            AlertDialog.show( title: "Error", body: StringDialog.dialog_error_server, view: self)
        }else {
            AlertDialog.show( title: "Error", body: errorMessage, view: self)
        }
    }
    
    public func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        var bh : BillingHistory?
        bh = BillingHistory()
        bh = item as? BillingHistory
        UIApplication.shared.openURL(NSURL(string: (bh?.url)!)! as URL)
    }
    
    @IBAction func payCardButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController:  PaymentViewController.self)
    }
   
}
