//
//  MainViewController.swift
//  MiEnlaceTP
//
//  Created by Jorge Hdez Villa on 06/01/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: MenuItemViewController,GetAccountStatusDelegate, CheckBalanceDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var packageLabe: UILabel!
    @IBOutlet weak var numberAccountLabel: UILabel!
    
    var mGetAccountPresenter : GetAccountStatusPresenter?
    var mGetBalancePresenter : BalancePresenter?
    var user : String?
    @IBOutlet weak var balanceTextField: UILabel!
    static var updateBalance : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Bienvenido"
        let realm = try! Realm()
        let user = realm.objects(User.self)
        mGetBalancePresenter = BalancePresenter(delegate: self)
        mGetAccountPresenter = GetAccountStatusPresenter(delegate: self)
        
        mGetBalancePresenter?.getBalance(account: user[0].accountNumber)
        mGetAccountPresenter?.stateAccount(No_Account: user[0].accountNumber)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(MainViewController.updateBalance == true){
            let realm = try! Realm()
            let user = realm.objects(User.self)
            mGetBalancePresenter?.getBalance(account: user[0].accountNumber)
            print("update")
        }
        self.navigationItem.title = "Bienvenido"
    }
    
    @IBAction func onStateAccountClick(_ sender: Any) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController: StateAccountViewController.self)
    }
    
    @IBAction func onPlanClick(_ sender: Any) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController: PlanContractedViewController.self)
    }
    
    @IBAction func onAttentionClick(_ sender: Any) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController: AttentionViewController.self)
    }

    @IBAction func onBalanceClick(_ sender: UIButton) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController:  PaymentViewController.self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onSendingAccount() {
        AlertDialog.showOverlay()
    }
    
    func onSendingCheckBalance() {
        
    }
    
    
    func onErrorCheckBalance(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    func onErrorConnectionCheckBalance(errorConnection: String) {
        AlertDialog.hideOverlay()
    }
    
    func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    func onErrorAccount(errorMessage: String) {
        AlertDialog.hideOverlay()
    }
    
    func onSuccessAccount(clientInfo: ClientInfo, infoAccount: AccountInfo, infoStatusAccount: AccountStatusInfo, billingHistory: [BillingHistory], bankReferences: BankReferences) {
        
        nameLabel.text = clientInfo.Nombre
        packageLabe.text = infoAccount.PaqueteContratado
        numberAccountLabel.text = "No.cuenta \(infoAccount.NoCuenta!)"
    
        let myVC = storyboard?.instantiateViewController(withIdentifier: "DrawerMenuViewController") as! DrawerMenuViewController
        myVC.addNameCustomer(name: clientInfo.Nombre!, plain: infoAccount.PaqueteContratado!)
        
        AlertDialog.hideOverlay()
    }
    
    func onSuccessCheckBalance(balance: String) {
        let morePrecisePI = Double(balance)
        let roundedF = CGFloat(ceil(Double(morePrecisePI!)))
        
        balanceTextField.text = "$\(String(format: "%.1f", roundedF))"
    }

}
