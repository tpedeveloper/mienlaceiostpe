//
//  BalanceDataSource.swift
//  MiEnlaceTP
//
//  Created by fer on 07/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

public protocol BalanceTableViewDelegate : NSObjectProtocol {
    func onItemClick(item: BankCard)
}


class DataSourceBalance: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items : [BankCard] = []
    var tableView : UITableView?
    var listadoTableViewDelegate : BalanceTableViewDelegate?
    
    init(tableView: UITableView, tableViewDelegate: BalanceTableViewDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.listadoTableViewDelegate = tableViewDelegate
    }
    
    func update(items: [BankCard]) {
        self.items = items
        self.tableView?.reloadData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "BankCardCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! BankCardCell
        
        let item = self.items[indexPath.row]
         cell.numberCardLabel.text = item.numberCard!
         cell.montTextField.text = item.validity!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        self.listadoTableViewDelegate?.onItemClick(item: item)
    }
}

