//
//  EditViewController.swift
//  MiEnlaceTP
//
//  Created by fer on 06/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class EditViewController: MenuItemViewController, GetInfoUserDelegate {
    var mGetInfoUserPresenter : GetInfoUserPresenter?

    @IBOutlet weak var TF_User: UITextField!
    @IBOutlet weak var TF_Name: UITextField!
    @IBOutlet weak var TF_LasName: UITextField!
    @IBOutlet weak var TF_Email: UITextField!
    @IBOutlet weak var TF_Enterprise: UITextField!
    @IBOutlet weak var TF_Area: UITextField!
    @IBOutlet weak var TF_Job: UITextField!
    @IBOutlet weak var TF_No_Employed: UITextField!
    @IBOutlet weak var TF_Sector: UITextField!
    
    var dataAccount : String?
    
    
    @IBAction func editButton(_ sender: UIButton) {
        ViewControllerUtils.pushViewController(viewController: self, newViewController: UpdateProfileViewController.self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        
        dataAccount = user[0].accountNumber
        
        // Do any additional setup after loading the view, typically from a nib.
        self.mGetInfoUserPresenter = GetInfoUserPresenter(delegate: self)
        self.mGetInfoUserPresenter?.getInfoUser(account: dataAccount!)
        
        TF_User.isUserInteractionEnabled = false
        TF_Name.isUserInteractionEnabled = false
        TF_LasName.isUserInteractionEnabled = false
        TF_Email.isUserInteractionEnabled = false
        TF_Enterprise.isUserInteractionEnabled = false
        TF_Area.isUserInteractionEnabled = false
        TF_Job.isUserInteractionEnabled = false
        TF_No_Employed.isUserInteractionEnabled = false
        TF_Sector.isUserInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let realm = try! Realm()
        let user = realm.objects(User.self)
        
        
        dataAccount = user[0].accountNumber
        
        // Do any additional setup after loading the view, typically from a nib.
        self.mGetInfoUserPresenter = GetInfoUserPresenter(delegate: self)
        self.mGetInfoUserPresenter?.getInfoUser(account: dataAccount!)

    }
    
    func onSuccessGetInfoUser(dataUser: UserData) {
        AlertDialog.hideOverlay()
        TF_User.text = dataUser.usrEmployeeNumber
        TF_Name.text = dataUser.usrFirstName
        TF_LasName.text = dataUser.usrLastName
        TF_Email.text = dataUser.usrEmail
        TF_Enterprise.text = dataUser.company
        TF_Area.text = dataUser.area
        TF_Job.text = dataUser.job
        TF_No_Employed.text = dataUser.employeesNumber
        TF_Sector.text = dataUser.industry
    }
    
    func onSendingGetInfoUser() {
        AlertDialog.showOverlay()
    }
    
    func onErrorConnectionGetInfoUser() {
        AlertDialog.hideOverlay()
    }
    
    func onErrorGetInfoUser(errorMessage: String) {
        AlertDialog.hideOverlay()
    }

}

