//
//  InfoCuenta.swift
//  MiEnlaceTP
//
//  Created by fer on 23/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class AccountInfo: NSObject, Mappable {
    
    var NoCuenta : String?
    var Dn : String?
    var PaqueteContratado : String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map : Map){
        NoCuenta <- map["NoCuenta"]
        Dn <- map["Dn"]
        PaqueteContratado <- map["PaqueteContratado"]
    }
}

