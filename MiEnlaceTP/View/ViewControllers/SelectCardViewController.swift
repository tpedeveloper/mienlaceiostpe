//
//  SelectCardViewController.swift
//  MiEnlaceTP
//
//  Created by fer on 28/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class SelectCardViewController : BaseViewController, SelectCardDelegate,TableViewCellClickDelegate{
    
    var mSelectCardPresenter : SelectCardPresenter?
    
    
    override func viewDidAppear(_ animated: Bool) {
        //AlertDialog.showOverlay()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mSelectCardPresenter = SelectCardPresenter(delegate: self)
        mSelectCardPresenter?.selectCard(account: "0101114571")
    }
    
    func onSuccessSelectCard() {
        
    }
    
    func onErrorConnection() {
        
    }
    
    func onSendingSelectCard() {
        
    }
    
    func onErrorSelectCard(errorMessage: String) {
        
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        
    }
}
