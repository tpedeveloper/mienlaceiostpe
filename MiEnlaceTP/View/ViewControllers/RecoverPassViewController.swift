//
//  RecoverPassViewController.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class RecoverPassViewController: BaseViewController, RecoverPasswordDelegate {

    @IBOutlet weak var Tf_No_Account: UITextField!
    
    var mRecoverDelegate : RecoverPasswordPresenter?

    
    
    public func onSuccessRecover(succesMessage: String) {
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Exito", body: succesMessage, view: self)
    }

    @IBAction func Btn_Recover(_ sender: UIButton) {
        if(Tf_No_Account.text == ""){
           AlertDialog.show( title: "Aviso", body: "Favor de ingresar un número de cuenta", view: self)
        }else{
            mRecoverDelegate?.recover(account: Tf_No_Account.text!, phone: "")
        }
        
    }
    
    public func onErrorRecover(errorMessage: String) {
        
        AlertDialog.hideOverlay()
        if errorMessage == "" {
            AlertDialog.show( title: "Error", body: StringDialog.dialog_error_server, view: self)
        } else {
            AlertDialog.show( title: "Error", body: errorMessage, view: self)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //AlertDialog.showOverlay()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mRecoverDelegate = RecoverPasswordPresenter(delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onSendingRecover() {
        AlertDialog.showOverlay()
    }

    
    func onErrorConnection(){
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error", body: StringDialog.dialog_error_connection, view: self)
    }

}
