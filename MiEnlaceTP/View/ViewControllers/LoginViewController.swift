//
//  LoginViewController.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import RealmSwift

class LoginViewController: BaseViewController, LoginDelegate, UITextFieldDelegate{
    
    
    @IBOutlet weak var tf_pass: UITextField!
    @IBOutlet weak var tf_user: UITextField!
    var mLoginPresenter : LoginPresenter?

    @IBAction func btn_ingresar(_ sender: UIButton) {
        if(tf_pass.text == "" || tf_user.text == "" ){
             AlertDialog.show( title: "Aviso", body: StringDialog.dialog_error_fields_empty, view: self)
        }else{
                AlertDialog.showOverlay()
                mLoginPresenter?.login(username: tf_user.text!, password: tf_pass.text!)
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            AlertDialog.showOverlay()
            mLoginPresenter?.login(username: tf_user.text!, password: tf_pass.text!)
            return false
        }
        return true
    }
    
    @IBAction func btn_recover_pass(_ sender: UIButton) {
        //ViewControllerUtils.presentViewController(from: self, to: RecoverPassViewController.self)
    }
    
    @IBAction func btn_register_account(_ sender: UIButton) {
        //ViewControllerUtils.presentViewController(from: self, to: RegisterViewController.self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mLoginPresenter = LoginPresenter(delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func onSendingLogin() {
        AlertDialog.showOverlay()
    }
    
    func onSuccessLogin(){
        let realm = try! Realm()
        
        try! realm.write {
            realm.deleteAll()
        }

        let user = User()
        user.accountNumber = tf_user.text!
        
        try! realm.write {
            realm.add(user)
        }
        
        //AlertDialog.hideOverlay()
        ViewControllerUtils.presentViewController(from: self, to: ContainerViewController.self)
    }
    
    
    func onErrorLogin(errorMessage : String){
        AlertDialog.hideOverlay()
        if errorMessage == "" {
            AlertDialog.show( title: "Error", body: StringDialog.dialog_error_server, view: self)
        } else {
            AlertDialog.show( title: "Error", body: errorMessage, view: self)
        }
    }
    
    
    func onErrorConnection(){
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error", body: StringDialog.dialog_error_connection, view: self)
    }
    
}
