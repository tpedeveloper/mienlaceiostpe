//
//  BasePresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import Realm

class BasePresenter: NSObject {
    
    override init(){
        super.init()
    }
    
    func loadPresenter(){
        
    }
    
    func unloadPresenter(){
        
    }
    
}
