//
//  File.swift
//  MiEnlaceTP
//
//  Created by Charls Salazar on 01/06/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class NewCarsAdd : NSObject, Mappable{
    var name: String?
    var lastName: String?
    var secondLastName: String?
    var cardNumber: String?
    var expirationMonth: String?
    var expirationYear: String?
    var amount: String?
    var cctype: String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        name		<- map["Name"]
        lastName		<- map["LastName"]
        secondLastName		<- map["SecondLastName"]
        cardNumber		<- map["CardNumber"]
        expirationMonth		<- map["ExpirationMonth"]
        expirationYear		<- map["ExpirationYear"]
        cctype		<- map["CCTYPE"]
    }
}
