//
//  GetInfoUserRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 02/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class GetInfoUserRequest: NSObject, Mappable {
    
    var login : Login?
    var accountNumber : String?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
        accountNumber <- map["Account"]
    }
}
