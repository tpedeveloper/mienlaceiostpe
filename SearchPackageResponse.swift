//
//  SearchPackageResponse.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchPackageResponse : NSObject, Mappable{
    var Result : LoginResult?
    dynamic var Plan:String?
    dynamic var Renta:String?
    dynamic var additionalFees: AdditionalFees?
    var services : [Service] = []
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Result		<- map["Result"]
        Plan		<- map["PLAN"]
        Renta		<- map["RENTA"]
        additionalFees		<- map["Tarifas_Adicionales_Al_Plan"]
        services		<- map["Servicios"]
    }

}
