//
//  GetAccountStatusInfoRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class GetAccountStatusInfoRequest : NSObject, Mappable{
    
    var login : Login?
    var accountNumber : String?
    
    override init() {
        super.init()
    }
    
    init(login: Login, accountNumber: String) {
        self.login = login
        self.accountNumber=accountNumber
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
        accountNumber <- map["NoAccount"]
    }
    
}
