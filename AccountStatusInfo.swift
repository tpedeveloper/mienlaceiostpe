//
//  AccountStatusInfo.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class AccountStatusInfo : NSObject, Mappable{
    dynamic var FechaPP:String?
    dynamic var PrecioPP:String?
    dynamic var FechaL:String?
    dynamic var PrecioL:String?
    dynamic var SaldoAnterior:String?
    dynamic var DescuentoPP:String?
    dynamic var OtrosCargosBonificaciones:String?
    dynamic var Pago:String?
    dynamic var SubTotalMesAnterior:String?
    dynamic var RentaPlan:String?
    dynamic var Consumos:String?
    dynamic var ServExtras:String?
    dynamic var Bonificaciones:String?
    dynamic var SubTotalMes:String?
    dynamic var SaldoTotalMes:String?
    dynamic var PeriodoInicio:String?
    dynamic var PeriodoFin:String?
    
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        FechaPP		<- map["FechaPP"]
        PrecioPP		<- map["PrecioPP"]
        FechaL		<- map["FechaL"]
        PrecioL		<- map["PrecioL"]
        SaldoAnterior		<- map["SaldoAnterior"]
        DescuentoPP		<- map["DescuentoPP"]
        OtrosCargosBonificaciones		<- map["OtrosCargosBonificacioes"]
        Pago		<- map["Pago"]
        SubTotalMesAnterior		<- map["SubTotalMesAnterior"]
        RentaPlan		<- map["RentaPlan"]
        Consumos		<- map["Consumos"]
        ServExtras		<- map["ServExtras"]
        Bonificaciones		<- map["Bonificaciones"]
        SubTotalMes		<- map["SubTotalMes"]
        SaldoTotalMes		<- map["SaldoTotalMes"]
        PeriodoInicio		<- map["PeriodoInicio"]
        PeriodoFin		<- map["PeriodoFin"]
        
    }
    
    
}
