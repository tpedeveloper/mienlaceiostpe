//
//  UpdateUserRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class UpdateUserRequest : NSObject, Mappable{
    var login : Login?
    var account : String?
    var uData : dataUser?
    
    override init() {
        super.init()
    }
    
    init(login: Login, account: String,userData: dataUser) {
        self.login = login
        self.account = account
        self.uData = userData
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        login <- map["Login"]
        account <- map["Account"]
        uData <- map["UserData"]
    }
}
