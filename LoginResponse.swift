//
//  LoginResponse.swift
//  MiEnlaceTP
//
//  Created by fer on 21/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginResponse  : NSObject, Mappable {
    
    var Result : LoginResult?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Result		<- map["Result"]
    }
    
}
