//
//  arrLoginRequest.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

class arrLoginRequest : NSObject, Mappable{
    var user : String?
    var password : String?
    var ip : String?
    
    override init() {
        super.init()
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map : Map){
        user <- map["User"]
        password <- map["Password"]
        ip <- map["Ip"]
    }

}
