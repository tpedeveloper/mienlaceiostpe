//
//  ClientInfo.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit
import ObjectMapper

public class ClientInfo : NSObject , Mappable{
    dynamic var Nombre : String?
    dynamic var Calle : String?
    dynamic var Numero : String?
    dynamic var Colonia : String?
    dynamic var Delegacion : String?
    dynamic var Ciudad : String?
    dynamic var Estado : String?
    dynamic var CP : String?
    dynamic var Pais : String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Nombre		<- map["Nombre"]
        Calle		<- map["Calle"]
        Colonia		<- map["Colonia"]
        Delegacion		<- map["Delegacion"]
        Ciudad		<- map["Ciudad"]
        Estado		<- map["Estado"]
        CP		<- map["CP"]
        Pais		<- map["Pais"]
    }
    
    
}
