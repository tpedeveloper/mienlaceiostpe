//
//  UpdateUserPresenter.swift
//  MiEnlaceTP
//
//  Created by fer on 22/02/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

class UpdateUserPresenter : BasePresenter {
    
    var mUpdateUserDelegate : UpdateUserDelegate?
    var request : Alamofire.Request?
    
    init(delegate : UpdateUserDelegate) {
        super.init()
        self.mUpdateUserDelegate = delegate
    }
    
    
    func updateInfo(account:String, userData: dataUser){
        
        if ConnectionUtils.isConnectedToNetwork(){
            
            self.mUpdateUserDelegate?.onSendingUpdateUser()
            
            let updateRequest : UpdateUserRequest = UpdateUserRequest()
            
            updateRequest.login = Login()
            updateRequest.login?.user = "25631"
            updateRequest.login?.password = "Middle100$"
            updateRequest.login?.ip = "1.1.1.1"
            updateRequest.account = account
            updateRequest.uData = userData
            
            let params  = Mapper().toJSON(updateRequest)
            
            request = Alamofire.request(Urls.API_UPDATE_USER, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: Urls.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<LoginResponse>) in
                
                switch response.result {
                case .success:
                    let objectReponse : LoginResponse = response.result.value!
                    if(objectReponse.Result?.result == "0" ){
                       self.mUpdateUserDelegate?.onSuccessUpdateUser(successMessage: "Datos Actualizados")
                    }else{
                        self.mUpdateUserDelegate?.onErrorUpdateUser(errorMessage: "Intente mas tarde por favor")
                    }
                case .failure(_): break
                self.mUpdateUserDelegate?.onErrorUpdateUser(errorMessage: "No se pudo contactar al servicio intente mas tarde")
                }
            }
            
        }else{
            self.mUpdateUserDelegate?.onErrorConnectionUpdateUser()
        }
    }

}

