//
//  BaseTableViewCell.swift
//  MiEnlaceTP
//
//  Created by fer on 10/03/17.
//  Copyright © 2017 Jorge Hdez Villa. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    var delegate : NSObjectProtocol!
    var itemObject : NSObject?
    
    func pupulate(object :NSObject) {
        preconditionFailure("This method must be overridden")
    }
    
    func executeAction() {
        preconditionFailure("This method must be overridden")
    }
    
    func toString() -> String{
        preconditionFailure("This method must be overridden")
    }
    
}
